<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                </div>
                <h4 class="page-title">Beranda</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-12">
            <div class="card card-body">
                <!-- <h1 class="card-title text-center mb-4">Struktur Organisasi</h1> -->
                <video autoplay muted loop id="yuriVideo">
                  <source src="<?=base_url('assets/images/');?>video_background2.mp4" type="video/mp4">
                </video>

            </div> <!-- end card-->
        </div>

        <div class="col-md-6">
            <div class="card card-body">
                <h1 class="card-title text-center">Visi</h1>

                <p class="card-tex text-center">" MENJADIKAN PURWAKARTA ISTIMEWA " </p>
<!--                 <p> Direktorat Jenderal Bina Marga mampu menyediakan jaringan jalan yang yang andal, terpadu &
                    berkelanjutan serta mampu mendukung pertumbuhan ekonomi dan kesejahteraan sosial demi tercapainya
                    Indonesia yang Aman, Adil dan Demokratis serta Lebih Sejahtera melalui pengaturan, pembinaan,
                    pembangunan, pengusahaan dan pengawasan yang meliputi wilayah Negara Kesatuan Republik Indonesia.
                </p> -->
            </div> <!-- end card-->
        </div>

        <div class="col-md-6">
            <div class="card card-body">
                <h1 class="card-title text-center">Misi</h1>

                <p class="card-text text-center">
                    Mewujudkan pembangunan infrastruktur dan pengembangan pariwisata berwawasan lingkungan yang berkelanjutan.
                </p>
            </div> <!-- end card-->
        </div>

        <div class="col-md-12">
            <div class="card card-body">
                <h1 class="card-title text-center mb-4">Struktur Organisasi</h1>
                <img src="<?=base_url('assets/images/');?>BAGAN.jpg" width="100%" height="1000">
            </div> <!-- end card-->
        </div>
    </div>
    <!-- end row -->
</div>
<!-- container -->
<script>
// Get the video
var video = document.getElementById("yuriVideo");

// Get the button
var btn = document.getElementById("yuriBtn");

// Pause and play the video, and change the button text
function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}
</script>