 <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Report monitoring.xls");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Realisasi - SIP Kab. Purwakarta</title>
    <link rel="stylesheet" href="https://sip.purwakartakab.go.id/assets/dashboard/bower/bootstrap/dist/css/bootstrap.min.css">
    <style type="text/css">
        a.comment-indicator:hover+comment {
            background: #ffd;
            position: absolute;
            display: block;
            border: 1px solid black;
            padding: 0.5em;
        }

        a.comment-indicator {
            background: red;
            display: inline-block;
            border: 1px solid black;
            width: 0.5em;
            height: 0.5em;
        }

        comment {
            display: none;
        }

        .tabel {
            border: 1px solid #000000 !important;
        }

        .top {
            margin-top: 15px;
        }

        .right {
            padding-right: 20px;
        }

        .text-bold {
            font-weight: bold;
        }
        
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #000000 !important;
        }

        @page {
            size: auto;
            margin: 5mm;
        }
    </style>
    
</head>

<body>
    <b style="text-center">DAFTAR REALISASI KEGIATAN BIDANG PEMELIHARAAN JALAN DAN JEMBATAN</b>
    <div class="text-bold">TAHUN ANGGARAN 2022</div>
    <table>
        <tr>
            <td class="right">BULAN&nbsp;</td>
            <td>&nbsp;: <span class="text-bold"><?=strtoupper($nama_bulan[$bulan])?></span>&nbsp;</td>
        </tr>
        <tr>
            <td class="right">SKPD&nbsp;</td>
            <td>&nbsp;: <span class="text-bold">DINAS PEKERJAAN UMUM DAN TATA RUANG</span>&nbsp;</td>
        </tr>
    </table>
    <table border="1" width="2000px;">
        <thead>
            <tr>
                <th width="200px" class="text-center" rowspan="2">KODE SUB KEGIATAN / KODE REKENING</th>
                <th width="350px" class="text-center" rowspan="2">SUB KEGIATAN</th>
                <th width="150px" class="text-center" rowspan="2">PAGU Rp. </th>
                <th width="100px" class="text-center" >Lokasi</th>
                <th width="300px" class="text-center" colspan="2">Target Penanganan</th>
                <th width="" class="text-center" rowspan="2">Jenis Konstruksi Permukaan</th>
                <th width="" class="text-center" rowspan="2"> Nilai Kontrak Rp.</th>
                <th width="" class="text-center" rowspan="2">Nomor dan Tgl Kontrak</th>
                <th width="" class="text-center" rowspan="2">Waktu Pelaksanaan (HK)</th>
                <th width="" class="text-center" colspan="2">SPMK</th>
                <th width="300px" class="text-center" rowspan="2">Penyedia Jasa</th>
                <th width="300px" class="text-center" rowspan="2">Pengawas</th>
                <th width="" class="text-center" rowspan="2">Rencana (%)</th>
                <th width="" class="text-center" rowspan="2">Realisasi (%)</th>
                <th width="" class="text-center" rowspan="2">Selisih (%)</th>
                <th width="300px" class="text-center" rowspan="2">Keterangan</th>
            </tr>
            <tr>
                <th class="text-center">Kecamatan</th>
                <th class="text-center">Panjang</th>
                <th class="text-center">Lebar</th>
                <th width="100px" class="text-center">Tanggal Mulai</th>
                <th width="100px" class="text-center">Tanggal Akhir</th>                
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($kegiatan->result() as $row) {
                    $proyek = $this->Kegiatan_M->get_kegiatan_proyek($row->id_sub_kegiatan);
                    if ($proyek->num_rows() != NULL) {
             ?>
                 <tr>
                     <td>&nbsp;<?php echo $row->kode_program.".".$row->kode_kegiatan.".".$row->kode_sub_kegiatan;?>&nbsp;</td>
                     <td>&nbsp;<?php echo $row->nama_sub_kegiatan;?>&nbsp;</td>
                     <td class="text-right">&nbsp;<?php echo number_format($row->pagu,0,',','.');?>&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                 </tr>
                <?php foreach ($proyek->result() as $data) { ?>
                 <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;<?php echo $data->ruas_jalan;?>&nbsp;</td>
                    <td class="text-right">&nbsp;<?php echo number_format($data->pagu_anggaran,0,',','.');?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->lokasi;?>&nbsp;</td>
                    <td class="text-right">&nbsp;<?php echo $data->panjang_ruas_jalan;?>&nbsp;</td>
                    <td class="text-right">&nbsp;<?php echo $data->lebar_ruas_jalan;?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->jenis_kontruksi;?>&nbsp;</td>
                    <td>&nbsp;<?php echo number_format($data->nilai_kontrak,0,',','.');?>&nbsp;</td>
                    <td class="text-center">&nbsp;<?php echo $data->nomor_spk;?><br>&nbsp;<?=date('d F Y',strtotime($data->tanggal_spk))?>&nbsp;</td>
                    <td class="text-right">&nbsp;<?php echo $data->waktu_pelaksanaan;?>&nbsp;</td>
                    <td class="text-center">&nbsp;<?php echo date('d-m-Y',strtotime($data->tanggal_mulai));?>&nbsp;</td>
                    <td class="text-center">&nbsp;<?php echo date('d-m-Y',strtotime($data->tanggal_akhir));?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->penyedia_jasa;?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->pengawas;?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->rencana;?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->persentase;?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->persentase-$data->rencana;?>&nbsp;</td>
                    <td>&nbsp;<?php echo $data->keterangan;?>&nbsp;</td>
                 </tr>
                <?php } ?>
                 <tr>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                 </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<!-- 
    <div class="col-md-12" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3 pull-right">
                <div class="text-center">Purwakarta, <?=date('d F Y')?> </div>
                <div class="text-center">Kepala Dinas Pekerjaan Umum dan Tata Ruang</div>
                <div class="text-center">Kabupaten Purwakarta</div>
                <div style="margin-top:80px">
                    <div class="text-center"><b><u>RYAN OKTAVIA, ST.MM.MT</u></b></div>
                    <div class="text-center">NIP. 19731017 199901 1 001</div>
                </div>
            </div>
        </div>
    </div>
 -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript">
        var css = '@page { size: landscape; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

        style.type = 'text/css';
        style.media = 'print';

        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);

        // window.print();
        window.onafterprint = back;

        function back() {
            window.history.back();
        }

        function getNum(text, comma = false) {
            var result = text.toString().trim();
            result = result.replaceAll('.', '');

            if (comma === true) {
                result = result.replaceAll(',', '.');
            }

            if (result == "") {
                return 0;
            }

            return Number(parseFloat(result));
        }

        function format(number) {
            return new Intl.NumberFormat('id-ID', {
                maximumFractionDigits: 0,
                minimumFractionDigits: 0,
                style: 'currency',
                currency: 'IDR',
                currencyDisplay: "code"
            })
            .format(number)
            .replace("IDR", "")
            .trim()
        }

        var sum1 = 0;
        var sum2 = 0;
        var sum4 = 0;
        var total = 0;
        $("#example1 tbody tr").each(function() {
            sum1 += getNum($(this).find("td:eq(2)").text());
            sum2 += getNum($(this).find("td:eq(3)").text());

            sum4 += getNum($(this).find("td:eq(5)").text(), true);
            total++;
        });
        $("#sum1").text(format(sum1));
        $("#sum2").text(format(sum2));
        $("#sum3").text((sum2 / sum1 * 100).toFixed(2).toString().replace('.', ','));
        $("#sum4").text((sum4 / total).toFixed(2).toString().replace('.', ','));
        // window.print();
    </script>
</body>
</html>