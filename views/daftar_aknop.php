<?php
    date_default_timezone_set("Asia/Bangkok");
    $tahun = date('Y',strtotime('-1 years'));
    // echo date_default_timezone_get();
?>
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a></li>
                        
                        <li class="breadcrumb-item active">Aknop</li>
                    </ol>
                </div>
                <h4 class="page-title">Data Aknop</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row mb-2"> 
        <?php if($this->session->tipe=='administrator'):?>
        <div class="col-sm-4">
            <a href="#modal_aknop" data-toggle="modal" id="btn-tambah-aknop" class="btn btn-danger btn-rounded mb-3"><i
                    class="mdi mdi-plus"></i> Buat
                Aknop</a>
            <!-- <a href="<?= base_url('admin/data_lengkap_proyek');?>" class="btn btn-primary btn-rounded mb-3"> -->
                <!-- Tampilan Lengkap -->
            <!-- </a> -->
        </div>
        <div class="col-sm-8">
            <div class="text-sm-right">
<!-- 
                <div class="btn-group mb-3">
                    <a href="<?=base_url('admin/data_proyek');?>" class="btn btn-primary">Semua</a>
                </div>
                <div class="btn-group mb-3 ml-1">
                    <a href="<?=base_url('admin/data_proyek_proses');?>" class="btn btn-light">Proses</a>
                    <a href="<?=base_url('admin/data_proyek_selesai');?>" class="btn btn-light">Selesai</a>
                </div>
 -->
                <!-- <div class="btn-group mb-3 ml-2 d-none d-sm-inline-block">
                    <button type="button" class="btn btn-dark"><i class="dripicons-view-apps"></i></button>
                </div>
                <div class="btn-group mb-3 d-none d-sm-inline-block">
                    <button type="button" class="btn btn-link text-dark"><i class="dripicons-checklist"></i></button>
                </div> -->
            </div>
        </div>
        <?php  endif; ?>
        <!-- end col-->
    </div>
    <!-- end row-->

    <div class="row">

        <div class="col-12">
            <?php if($this->session->flashdata('success')):?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $this->session->flashdata('success');?>
            </div>
        <?php endif;?>
            <div class="card">
                <div class="card-body">

                    <table id="data_lengkap" class="table dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">Nama Daerah Irigasi</th>
                                <th rowspan="2">Tahun Anggaran</th>
                                <th colspan="3" class="text-center">Aknop Rencana </th>
                                <th rowspan="2">Total Aknop</th>
                                <th rowspan="2">Indeks</th>
                                <?php if($this->session->tipe=='administrator'):?>
                                <th rowspan="2">Tindakan</th>
                               <?php endif; ?>
                            </tr>
                            <tr>
                                <th>Operasi</th>
                                <th>Rutin</th>
                                <th>Berkala</th>
                            </tr>
                        </thead>


                        <tbody>
                            <?php $n=1; foreach($aknop->result() as $row):?>
                            <?php 
                                $prasarana_fisik         = (($row->prasarana_fisik * 45) / 100);
                                $sarana_penunjang        = (($row->sarana_penunjang * 10) / 100);
                                $produktifitas_tanam     = (($row->produktifitas_tanam * 15) / 100);
                                $organisasi_personalia   = (($row->organisasi_personalia * 15) / 100);
                                $dokumentasi             = (($row->dokumentasi * 5) / 100);
                                $p3a                     = (($row->p3a * 10) / 100);
                                $total_indeks = $prasarana_fisik+$sarana_penunjang+$produktifitas_tanam+$organisasi_personalia+$dokumentasi+$p3a;
                            ?>
                            <tr>
                                <td><?php echo $n++;?></td>
                                <td>
                                    <a href="#modal_aknop" data-toggle="modal" data-id="<?=$row->id_aknop?>" onclick="detail_aknop()" class="detail_aknop">
                                        <?=$row->nama_di?>
                                    </a>
                                </td>
                                <td><?=$row->tahun_anggaran?></td>
                                <td>Rp. <?php echo number_format($row->anggaran_operasional+$row->anggaran_aktor,0,',','.');?></td>
                                <td>Rp. <?php echo number_format($row->anggaran_perbaikan,0,',','.');?></td>
                                <td>Rp. <?php echo number_format($row->anggaran_pergantian,0,',','.');?></td>
                                <td>Rp. <?php echo number_format($row->anggaran_operasional+$row->anggaran_aktor+$row->anggaran_perbaikan+$row->anggaran_pergantian,0,',','.');?></td>
                                <td><?=$total_indeks?></td>
                                <?php if($this->session->tipe=='administrator'):?>
                                <td>
                                    <a href="#modal_aknop" data-toggle="modal" class="btn btn-icon btn-info btn-sm btn-edit" onclick="edit_aknop(<?=$row->id_aknop?>)" data-id="<?=$row->id_aknop?>" data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title="Edit"> <i
                                            class="mdi mdi-account-edit"></i> </a>
                                    <a href="#" class="btn btn-icon btn-danger btn-sm btn-delete" data-id="<?=$row->id_aknop?>" data-aknop="<?=$row->nama_di?>" data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title="Hapus"> <i
                                            class="mdi mdi-account-remove"></i>
                                    </a>
                                </td>
                            <?php endif; ?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

    
                    </li>
                </ul>
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row-->



</div> <!-- container -->


<div id="modal_aknop" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left">Form Aknop</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="form-aknop">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="namaLengkap" class="col-form-label">Nama Irigasi</label>
                    <input type="hidden" name="id_aknop" id="id_aknop" value="">
                    <select  class="form-control" id="nama_irigasi" name="nama_irigasi" data-placeholder="Nama Daerah Irigasi">
                        <!-- <option>Pilih Irigasi</option> -->
                        <?php foreach ($irigasi->result() as $value) { ?>
                            <option value="<?=$value->nama_irigasi?>"><?=$value->nama_irigasi?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="namaLengkap" class="col-form-label">Tahun Anggaran</label>
                    <select id="tahun_anggaran" name="tahun_anggaran" class="form-control" required style="width: 100%;" data-placeholder="Tahun . . .">
                        <option></option>
                        <?php for ($i=0; $i <5 ; $i++) { ?>
                            <option value="<?=$tahun?>"><?=$tahun++?></option>
                        <?php } ?>
                    </select>
<!--                     <input type="month" class="form-control" id="tahun_anggaran" name="tahun_anggaran"
                        placeholder="Tahun Anggaran "> -->
                </div>
                <div class="form-group col-md-6">
                    <!-- Rounded switch -->
                    <label for="namaLengkap" class="col-form-label">Kategori</label>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="switch">
                              <input type="checkbox" id="kategori_aknop" class="form-control" value="operasional" name="kategori_aknop" checked>
                              <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="col-sm-10">
                            <h5>Operasi & Pemeliharaan</h5>
                        </div>
                    </div>

                </div>
                <div class="form-group col-md-4">
                    <label for="namaLengkap" class="col-form-label">Kondisi Prasarana Fisik</label>
                    <input type="number" class="form-control buat_nilai" id="prasarana_fisik" name="prasarana_fisik" max="100" 
                        placeholder="Nilai Kondisi Prasarana Fisik">
                </div>
                <div class="form-group col-md-4">
                    <label for="namaLengkap" class="col-form-label">Sarana Penunjang</label>
                    <input type="number" class="form-control buat_nilai" id="sarana_penunjang" name="sarana_penunjang" max="100" 
                        placeholder="Nilai Sarana Penunjang">
                </div>
                <div class="form-group col-md-4">
                    <label for="namaLengkap" class="col-form-label">Produktifitas Tanam</label>
                    <input type="number" class="form-control buat_nilai" id="produktifitas_tanam" name="produktifitas_tanam" max="100" 
                        placeholder="Nilai Produktifitas Tanam">
                </div>
                <div class="form-group col-md-4">
                    <label for="namaLengkap" class="col-form-label">Organisasi Personalia</label>
                    <input type="number" class="form-control buat_nilai" id="organisasi_personalia" name="organisasi_personalia" max="100" 
                        placeholder="Nilai Organisasi Personalia">
                </div>
                <div class="form-group col-md-4">
                    <label for="namaLengkap" class="col-form-label">Dokumentasi</label>
                    <input type="number" class="form-control buat_nilai" id="dokumentasi" name="dokumentasi" max="100" 
                        placeholder="Nilai Dokumentasi">
                </div>
                <div class="form-group col-md-4">
                    <label for="namaLengkap" class="col-form-label">Kondisi P3A</label>
                    <input type="number" class="form-control buat_nilai" id="p3a" name="p3a" max="100" 
                        placeholder="Nilai Kondisi P3A">
                </div>

                <div class="col-sm-12">
                    <hr>
                </div>

                <div class="col-sm-12 operasional">
                    <label for="namaLengkap" class="col-form-label">A. Rencana Kegiatan Operasi & Pemeliharaan</label>
                    <hr>
                    <div class="row form-group">
                        <div class="row form-group col-sm-12">
                            <label for="namaLengkap" class="col-form-label col-md-5">Biaya Operasional</label>
                            <div class="row col-md-7">
                                <input type="hidden" class="form-control" id="anggaran_operasional" name="anggaran_operasional"
                                    placeholder="RP. " value="0">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="pelaku" name="pelaku[]"
                                placeholder="Uraian">
                        </div>
                        <div class="col-md-4">
                            <input type="number" class="form-control" id="anggaran_pelaku" name="anggaran_pelaku[]"
                                placeholder="RP.">
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-xs" id="btn-tambah-pelaku"><i class="mdi mdi-plus"></i></button>
                        </div>
                    </div>
                    <p id="kolom_pelaku"></p>
                </div>

                <div class="col-sm-12">
                    <hr>
                </div>

                <div class="col-sm-12 perbaikan">
                    <label for="namaLengkap" class="col-form-label col-md-9">B. Pemeliharaan Rutin</label>
                    <hr>
                    <input type="text" class="form-control" id="anggaran_perbaikan" name="anggaran_perbaikan"
                        placeholder="RP. ">
                </div>

                <div class="col-sm-12">
                    <hr>
                </div>

                <div class="col-sm-12 perbaikan">
                    <label for="namaLengkap" class="col-form-label col-md-9">C. Pemeliharaan Berkala</label>
                    <hr>
                    <input type="text" class="form-control" id="anggran_pergantian" name="anggaran_pergantian"
                        placeholder="RP. ">
                </div>

            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn-simpan" class="btn btn-success" >Simpan</button>
        <button type="button" id="btn-update" class="btn btn-warning" >Simpan</button>
      </div>
    </div>

  </div>
</div>


<script src="<?=base_url();?>assets/js/jquery/jquery.js"></script>
<script type="text/javascript">
// $('#kategori_aknop').attr('checked',true);
// $('.perbaikan').show();
$(document).ready(function () {

    $('#btn-tambah-aknop').on('click',function() {
        $('#btn-tambah-pelaku').show();
        $('#btn-simpan').show();
        $('#btn-update').hide();

        $('#id_aknop').val('');
        $('#nama_irigasi').val('');
        $('#tahun_anggaran').val('');
        $('#prasarana_fisik').val('');
        $('#sarana_penunjang').val('');
        $('#produktifitas_tanam').val('');
        $('#organisasi_personalia').val('');
        $('#dokumentasi').val('');
        $('#p3a').val('');
        $('#anggaran_operasional').val('');
        $('#pelaku').val('');
        $('#anggaran_pelaku').val('');

        $('#kategori_aknop').attr('checked',true);
        $('#anggaran_perbaikan').val('');
        $('#anggran_pergantian').val('');
        $('.perbaikan').show();

        $('#kolom_pelaku').html('');
        $(".form-control").attr("disabled",false);
    })

    $('#kategori_aknop').on('change',function() {
        if ($('#kategori_aknop').is(':checked')) {
            $('.perbaikan').fadeIn();
        }else{
            $('#anggaran_perbaikan').val('');
            $('#anggran_pergantian').val('');
            $('.perbaikan').hide();
        }
    })

    $('.buat_nilai').on('change',function() {
        if ($('#prasarana_fisik').val() > 100 || $('#sarana_penunjang').val() > 100 || $('#produktifitas_tanam').val() > 100 || $('#organisasi_personalia').val() > 100 || $('#dokumentasi').val() > 100 || $('#p3a').val() > 100) {
            Swal.fire('PERINGATAN!!', "Nilai yang di input tidak boleh lebih dari 100", 'warning')
        }else{
            var prasarana_fisik         = (($('#prasarana_fisik').val() * 45) / 100);
            var sarana_penunjang        = (($('#sarana_penunjang').val() * 10) / 100);
            var produktifitas_tanam     = (($('#produktifitas_tanam').val() * 15) / 100);
            var organisasi_personalia   = (($('#organisasi_personalia').val() * 15) / 100);
            var dokumentasi             = (($('#dokumentasi').val() * 5) / 100);
            var p3a                     = (($('#p3a').val() * 10) / 100);
            var total = prasarana_fisik+sarana_penunjang+produktifitas_tanam+organisasi_personalia+dokumentasi+p3a;

            console.log(total)

            if(total < 55)
            {
                $('#anggaran_perbaikan').val('');
                $('#anggran_pergantian').val('');
                $('#kategori_aknop').attr('checked',true);
                // $('#kategori_aknop').attr('disabled',true);
                $('.perbaikan').fadeIn();
            }else{
                $('#kategori_aknop').attr('checked',false);
                $('.perbaikan').hide();
                // $('#kategori_aknop').attr('disabled',false);
            }
        }
    })

    $('#btn-tambah-pelaku').on('click',function() {
        var kolom = $('.kolom-pelaku').length;
        var kolom_baru = kolom+1;
        console.log(kolom_baru)
        var html = '';
        html +='<div class="row form-group kolom-pelaku" data-kolom="'+kolom_baru+'">';
        html +='    <div class="col-md-6">';
        html +='        <input type="text" class="form-control" name="pelaku[]" placeholder="Pelaku">';
        html +='    </div>';
        html +='    <div class="col-md-4">';
        html +='        <input type="number" class="form-control" name="anggaran_pelaku[]" placeholder="Anggaran">';
        html +='    </div>';
        html +='    <div class="col-md-2">';
        html +='        <button type="button" class="btn btn-danger btn-xs" onclick="hapus_kolom('+kolom_baru+')"><i class="mdi mdi-minus"></i></button>';
        html +='    </div>';
        html +='</div>';
        $('#kolom_pelaku').append(html);
    });

    $('#btn-simpan').on('click',function() {
        var data = $('#form-aknop').serialize();
        loading();
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/Aknop/simpan_aknop')?>', // replace with your server-side script URL
        data: data,
        encode: true,
          success: function(result){
            if (result == "Berhasil") {
                Swal.fire({
                          title: "SUCCESS",
                          text: "Data Berhasil Di Simpan",
                          icon: "success",
                          showConfirmButton: false,
                          timer: 3000,
                        }).then(() => {
                  window.location.href="<?=base_url()?>admin/aknop/success";
                });
            }else{
                Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
            }
          }
        });
    })

    $('#btn-update').on('click',function() {
        var data = $('#form-aknop').serialize();
        loading();
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/Aknop/update_aknop')?>', // replace with your server-side script URL
        data: data,
        encode: true,
          success: function(result){
            if (result == "Berhasil") {
                Swal.fire({
                          title: "SUCCESS",
                          text: "Data Berhasil Di Simpan",
                          icon: "success",
                          showConfirmButton: false,
                          timer: 3000,
                        }).then(() => {
                  window.location.href="<?=base_url()?>admin/aknop/success";
                });
            }else{
                Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
            }
          }
        });
    })

    $('.detail_aknop').on('click',function() {
        var id_aknop = $(this).attr('data-id');
        $('#kolom_pelaku').html('');
        var html = '';
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/Aknop/get_aknop_ajax')?>', // replace with your server-side script URL
        data: {id_aknop:id_aknop},
        encode: true,
          success: function(result){
            var data = JSON.parse(result);

            $('#btn-tambah-pelaku').hide();
            $('#btn-simpan').hide();
            $('#btn-update').hide();

            $('#id_aknop').val(data[0].id_aknop);
            $('#nama_irigasi').val(data[0].nama_di);
            $('#tahun_anggaran').val(data[0].tahun_anggaran);
            $('#prasarana_fisik').val(data[0].prasarana_fisik);
            $('#sarana_penunjang').val(data[0].sarana_penunjang);
            $('#produktifitas_tanam').val(data[0].produktifitas_tanam);
            $('#organisasi_personalia').val(data[0].organisasi_personalia);
            $('#dokumentasi').val(data[0].dokumentasi);
            $('#p3a').val(data[0].p3a);
            $('#anggaran_operasional').val(data[0].anggaran_operasional);
            $('#pelaku').val(data[0].nama_aktor);
            $('#anggaran_pelaku').val(data[0].anggaran_aktor);

            if (data[0].kategori_aknop == 1) {
                $('#kategori_aknop').attr('checked',true);
                $('#anggaran_perbaikan').val(data[0].anggaran_perbaikan);
                $('#anggran_pergantian').val(data[0].anggaran_pergantian);
                $('.perbaikan').show();
            }else{
                $('#kategori_aknop').attr('checked',false);
                $('.perbaikan').hide();
            }

            for (var i = 1; i < data.length; i++) {
                html +='<div class="row form-group kolom-pelaku" data-kolom="'+i+'">';
                html +='    <div class="col-md-6">';
                html +='        <input type="text" class="form-control" name="pelaku[]" value="'+data[i].nama_aktor+'" placeholder="Pelaku">';
                html +='    </div>';
                html +='    <div class="col-md-4">';
                html +='        <input type="number" class="form-control" name="anggaran_pelaku[]" value="'+data[i].anggaran_aktor+'" placeholder="Anggaran">';
                html +='    </div>';
                html +='</div>';
            }
            $('#kolom_pelaku').append(html);
            $(".form-control").attr("disabled",true);
            // $('#nama_irigasi').val(data[0].nama_di);
          }
      });
    })


    $('.btn-edit').on('click',function() {
        var id_aknop = $(this).attr('data-id');
        $('#kolom_pelaku').html('');
        var html = '';
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/Aknop/get_aknop_ajax')?>', // replace with your server-side script URL
        data: {id_aknop:id_aknop},
        encode: true,
          success: function(result){
            var data = JSON.parse(result);

            $('#btn-tambah-pelaku').show();
            $('#btn-simpan').hide();
            $('#btn-update').show();

            $('#id_aknop').val(data[0].id_aknop);
            $('#nama_irigasi').val(data[0].nama_di);
            $('#tahun_anggaran').val(data[0].tahun_anggaran);
            $('#prasarana_fisik').val(data[0].prasarana_fisik);
            $('#sarana_penunjang').val(data[0].sarana_penunjang);
            $('#produktifitas_tanam').val(data[0].produktifitas_tanam);
            $('#organisasi_personalia').val(data[0].organisasi_personalia);
            $('#dokumentasi').val(data[0].dokumentasi);
            $('#p3a').val(data[0].p3a);
            $('#anggaran_operasional').val(data[0].anggaran_operasional);
            $('#pelaku').val(data[0].nama_aktor);
            $('#anggaran_pelaku').val(data[0].anggaran_aktor);

            if (data[0].kategori_aknop == 1) {
                $('#kategori_aknop').attr('checked',true);
                $('#anggaran_perbaikan').val(data[0].anggaran_perbaikan);
                $('#anggran_pergantian').val(data[0].anggaran_pergantian);
                $('.perbaikan').show();
            }else{
                $('#kategori_aknop').attr('checked',false);
                $('.perbaikan').hide();
            }

            for (var i = 1; i < data.length; i++) {
                html +='<div class="row form-group kolom-pelaku" data-kolom="'+i+'">';
                html +='    <div class="col-md-6">';
                html +='        <input type="text" class="form-control" name="pelaku[]" value="'+data[i].nama_aktor+'" placeholder="Pelaku">';
                html +='    </div>';
                html +='    <div class="col-md-4">';
                html +='        <input type="number" class="form-control" name="anggaran_pelaku[]" value="'+data[i].anggaran_aktor+'" placeholder="Anggaran">';
                html +='    </div>';
                html +='    <div class="col-md-2">';
                html +='        <button type="button" class="btn btn-danger btn-xs" onclick="hapus_kolom('+i+')"><i class="mdi mdi-minus"></i></button>';
                html +='    </div>';
                html +='</div>';
            }
            $('#kolom_pelaku').append(html);
            $(".form-control").attr("disabled",false);
            // $('#nama_irigasi').val(data[0].nama_di);
          }
      });
    })

    $('.btn-delete').on('click',function() {
        var id_aknop = $(this).attr('data-id');
        var nama_aknop = $(this).attr('data-aknop');
        Swal.fire({
          title: 'Apakah anda yakin menghapus Data '+nama_aknop+' ?',
          text: "Anda tidak dapat mengembalikan data yang sudah di hapus !!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Aknop/delete_aknop')?>', // replace with your server-side script URL
            data: {id_aknop:id_aknop},
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/aknop/success_delete";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
          }
        })
    })
});    

</script>



<script>
function loading() {
    Swal.fire({
      // title: 'Sweet!',
      // text: 'Modal with a custom image.',
      imageUrl: '<?=base_url();?>assets/images/loading.gif',
      showConfirmButton: false,
      imageWidth: 200,
      imageHeight: 200,
      // imageAlt: 'Custom image',
    })
}

function hapus_kolom(kolom) {
    var nama_kolom = '[data-kolom='+kolom+']';
    console.log(nama_kolom)
    $(nama_kolom).remove()
}

function delete_proyek(id_monitoring) {
        // var id_monitoring = $(this).attr('data-id');
        Swal.fire({
          title: 'Apakah anda yakin menghapus data ini ?',
          text: "Anda tidak dapat mengembalikan data yang sudah di hapus !!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proyek/delete_proyek')?>', // replace with your server-side script URL
            data: {id_monitoring:id_monitoring},
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/aknop/success_delete";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
          }
        })
    }    
</script>
<script>
    function detail_aknop(id_aknop) {
        var id_aknop = $(this).attr('data-id');
        $('#kolom_pelaku').html('');
        var html = '';
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/Aknop/get_aknop_ajax')?>', // replace with your server-side script URL
        data: {id_aknop:id_aknop},
        encode: true,
          success: function(result){
            var data = JSON.parse(result);

            $('#btn-tambah-pelaku').hide();
            $('#btn-simpan').hide();
            $('#btn-update').hide();

            $('#id_aknop').val(data[0].id_aknop);
            $('#nama_irigasi').val(data[0].nama_di);
            $('#tahun_anggaran').val(data[0].tahun_anggaran);
            $('#prasarana_fisik').val(data[0].prasarana_fisik);
            $('#sarana_penunjang').val(data[0].sarana_penunjang);
            $('#produktifitas_tanam').val(data[0].produktifitas_tanam);
            $('#organisasi_personalia').val(data[0].organisasi_personalia);
            $('#dokumentasi').val(data[0].dokumentasi);
            $('#p3a').val(data[0].p3a);
            $('#anggaran_operasional').val(data[0].anggaran_operasional);
            $('#pelaku').val(data[0].nama_aktor);
            $('#anggaran_pelaku').val(data[0].anggaran_aktor);

            if (data[0].kategori_aknop == 1) {
                $('#kategori_aknop').attr('checked',true);
                $('#anggaran_perbaikan').val(data[0].anggaran_perbaikan);
                $('#anggran_pergantian').val(data[0].anggaran_pergantian);
                $('.perbaikan').show();
            }else{
                $('#kategori_aknop').attr('checked',false);
                $('.perbaikan').hide();
            }

            for (var i = 1; i < data.length; i++) {
                html +='<div class="row form-group kolom-pelaku" data-kolom="'+i+'">';
                html +='    <div class="col-md-6">';
                html +='        <input type="text" class="form-control" name="pelaku[]" value="'+data[i].nama_aktor+'" placeholder="Pelaku">';
                html +='    </div>';
                html +='    <div class="col-md-4">';
                html +='        <input type="number" class="form-control" name="anggaran_pelaku[]" value="'+data[i].anggaran_aktor+'" placeholder="Anggaran">';
                html +='    </div>';
                html +='</div>';
            }
            $('#kolom_pelaku').append(html);
            $(".form-control").attr("disabled",true);
            // $('#nama_irigasi').val(data[0].nama_di);
          }
      });
    }



    function edit_aknop(id_aknop) {
        // var id_aknop = $(this).attr('data-id');
        $('#kolom_pelaku').html('');
        var html = '';
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/Aknop/get_aknop_ajax')?>', // replace with your server-side script URL
        data: {id_aknop:id_aknop},
        encode: true,
          success: function(result){
            var data = JSON.parse(result);

            $('#btn-tambah-pelaku').show();
            $('#btn-simpan').hide();
            $('#btn-update').show();

            $('#id_aknop').val(data[0].id_aknop);
            $('#nama_irigasi').val(data[0].nama_di);
            $('#tahun_anggaran').val(data[0].tahun_anggaran);
            $('#prasarana_fisik').val(data[0].prasarana_fisik);
            $('#sarana_penunjang').val(data[0].sarana_penunjang);
            $('#produktifitas_tanam').val(data[0].produktifitas_tanam);
            $('#organisasi_personalia').val(data[0].organisasi_personalia);
            $('#dokumentasi').val(data[0].dokumentasi);
            $('#p3a').val(data[0].p3a);
            $('#anggaran_operasional').val(data[0].anggaran_operasional);
            $('#pelaku').val(data[0].nama_aktor);
            $('#anggaran_pelaku').val(data[0].anggaran_aktor);

            if (data[0].kategori_aknop == 1) {
                $('#kategori_aknop').attr('checked',true);
                $('#anggaran_perbaikan').val(data[0].anggaran_perbaikan);
                $('#anggran_pergantian').val(data[0].anggaran_pergantian);
                $('.perbaikan').show();
            }else{
                $('#kategori_aknop').attr('checked',false);
                $('.perbaikan').hide();
            }

            for (var i = 1; i < data.length; i++) {
                html +='<div class="row form-group kolom-pelaku" data-kolom="'+i+'">';
                html +='    <div class="col-md-6">';
                html +='        <input type="text" class="form-control" name="pelaku[]" value="'+data[i].nama_aktor+'" placeholder="Pelaku">';
                html +='    </div>';
                html +='    <div class="col-md-4">';
                html +='        <input type="number" class="form-control" name="anggaran_pelaku[]" value="'+data[i].anggaran_aktor+'" placeholder="Anggaran">';
                html +='    </div>';
                html +='    <div class="col-md-2">';
                html +='        <button type="button" class="btn btn-danger btn-xs" onclick="hapus_kolom('+i+')"><i class="mdi mdi-minus"></i></button>';
                html +='    </div>';
                html +='</div>';
            }
            $('#kolom_pelaku').append(html);
            $(".form-control").attr("disabled",false);
            // $('#nama_irigasi').val(data[0].nama_di);
          }
      });
    }
</script>