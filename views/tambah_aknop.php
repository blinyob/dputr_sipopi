<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url('admin/Aknop');?>">Aknop</a></li>
                        <li class="breadcrumb-item active">Tambah Aknop</li>
                    </ol>
                </div>
                <h4 class="page-title">Form Tambah Data Aknop</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- Form row -->
    <div class="row">
        <div class="col-12">
            <?php if($this->session->flashdata('error')):?>
            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url('proses/tambah_Aknop');?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="namaLengkap" class="col-form-label">Kondisi Prasarana Fisik</label>
                                <input type="number" class="form-control" id="prasarana_fisik" name="prasarana_fisik"
                                    placeholder="Nilai Kondisi Prasarana Fisik">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="namaLengkap" class="col-form-label">Sarana Penunjang</label>
                                <input type="number" class="form-control" id="sarana_penunjang" name="sarana_penunjang"
                                    placeholder="Nilai Sarana Penunjang">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="namaLengkap" class="col-form-label">Produktifitas Tanam</label>
                                <input type="number" class="form-control" id="produktifitas_tanam" name="produktifitas_tanam"
                                    placeholder="Nilai Produktifitas Tanam">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="namaLengkap" class="col-form-label">Organisasi Personalia</label>
                                <input type="number" class="form-control" id="organisasi_personalia" name="organisasi_personalia"
                                    placeholder="Nilai Organisasi Personalia">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="namaLengkap" class="col-form-label">Dokumentasi</label>
                                <input type="number" class="form-control" id="dokumentasi" name="dokumentasi"
                                    placeholder="Nilai Dokumentasi">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="namaLengkap" class="col-form-label">Kondisi P3A</label>
                                <input type="number" class="form-control" id="p3a" name="p3a"
                                    placeholder="Nilai Kondisi P3A">
                            </div>

                            <div class="form-group col-md-12">
                                <hr>
                            </div>

                            <div class="form-group card col-md-12">
                                <label for="namaLengkap" class="col-form-label col-md-9">Rencana Kegiatan Operasi & Pemeliharaan</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="p3a" name="p3a"
                                            placeholder="Pelaku">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" id="p3a" name="p3a"
                                            placeholder="Anggaran">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-success btn-xs"><i class="mdi mdi-plus"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <hr>
                            </div>

                            <div class="form-group card col-md-12">
                                    <label for="namaLengkap" class="col-form-label col-md-9">Pemeliharaan</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="p3a" name="p3a"
                                            placeholder="Pelaku">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" id="p3a" name="p3a"
                                            placeholder="Anggaran">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-success btn-xs"><i class="mdi mdi-plus"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <hr>
                            </div>

                            <div class="form-group card col-md-12">
                                    <label for="namaLengkap" class="col-form-label col-md-9">Pemeliharaan</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="p3a" name="p3a"
                                            placeholder="Pelaku">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" id="p3a" name="p3a"
                                            placeholder="Anggaran">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-success btn-xs"><i class="mdi mdi-plus"></i></button>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group col-md-12">
                                <hr>
                            </div>

                        </div>

                        <a href="<?=base_url('admin/data_proyek');?>" class="btn btn-primary float-left"><i
                                class="mdi mdi-undo"></i> Kembali </a>
                        <button type="submit" class="btn btn-success float-right"><i class="mdi mdi-floppy"></i> Simpan
                        </button>

                    </form>

                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row -->
</div> <!-- container -->