<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a></li>
                        
                        <li class="breadcrumb-item active">Proyek</li>
                    </ol>
                </div>
                <h4 class="page-title">Data Proyek</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row mb-2"> 
        <?php if($this->session->tipe=='administrator'):?>
        <div class="col-sm-4">
            <a href="<?= base_url('admin/tambah_data_proyek');?>" class="btn btn-danger btn-rounded mb-3"><i
                    class="mdi mdi-plus"></i> Buat
                Proyek</a>
            <a href="<?= base_url('admin/data_lengkap_proyek');?>" class="btn btn-primary btn-rounded mb-3">
                Tampilan Lengkap
            </a>
        </div>
        <div class="col-sm-8">
            <div class="text-sm-right">
                <div class="btn-group mb-3">
                    <a href="<?=base_url('admin/data_proyek');?>" class="btn btn-primary">Semua</a>
                </div>
                <div class="btn-group mb-3 ml-1">
                    <a href="<?=base_url('admin/data_proyek_proses');?>" class="btn btn-light">Proses</a>
                    <a href="<?=base_url('admin/data_proyek_selesai');?>" class="btn btn-light">Selesai</a>
                </div>
                <!-- <div class="btn-group mb-3 ml-2 d-none d-sm-inline-block">
                    <button type="button" class="btn btn-dark"><i class="dripicons-view-apps"></i></button>
                </div>
                <div class="btn-group mb-3 d-none d-sm-inline-block">
                    <button type="button" class="btn btn-link text-dark"><i class="dripicons-checklist"></i></button>
                </div> -->
            </div>
        </div>
        <?php  endif; ?>
        <!-- end col-->
    </div>
    <!-- end row-->

    <div class="row">

        <div class="col-12">
            <?php if($this->session->flashdata('success')):?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $this->session->flashdata('success');?>
            </div>
        <?php endif;?>
            <div class="card">
                <div class="card-body">

                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Sub Kegiatan</th>
                                <th>No. SPK</th>
                                <th>Ruas Jalan </th>
                                <th>Lokasi</th>
                                <th></th>
                                <?php if($this->session->tipe=='administrator'):?>
                                <th>Tindakan</th>
                               <?php endif; ?>
                            </tr>
                        </thead>


                        <tbody>
                            <?php $n=1; foreach($data_proyek->result_array() as $proyek):?>
                            <tr>
                                <td><?php echo $n++;?></td>
                                <td><?php echo $proyek['kode_program'].".".$proyek['kode_kegiatan'].".".$proyek['kode_sub_kegiatan'];?></td>
                                <td><a href="<?= base_url('admin/detail_proyek/');?><?php echo $proyek['id_monitoring'];?>"><?php echo $proyek['nomor_spk'];?></a></td>
                                <td><?php echo $proyek['ruas_jalan'];?></td>
                                <td><?php echo $proyek['lokasi'];?></td>
                                <td>
                                    <!-- project progress-->
                                    <p class="mb-2 font-weight-bold">Progress <span class="float-right"><?=number_format($proyek['persentase'],0,'.',',');?>%</span></p>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$proyek['persentase'];?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$proyek['persentase'];?>%;">
                                        </div><!-- /.progress-bar -->
                                    </div><!-- /.progress -->
                                </td>
                                <?php if($this->session->tipe=='administrator'):?>
                                <td>
                                    <a href="<?= base_url('admin/edit_proyek/');?><?php echo $proyek['id_monitoring'];?>" class="btn btn-icon btn-info btn-sm" data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title="Edit"> <i
                                            class="mdi mdi-account-edit"></i> </a>
                                    <a href="#" class="btn btn-icon btn-danger btn-sm" data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title="Hapus" onclick="delete_proyek(<?=$proyek['id_monitoring']?>,'<?=$proyek['nomor_spk']?>');"> <i
                                            class="mdi mdi-account-remove"></i>
                                    </a>
                                </td>
                            <?php endif; ?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

    
                    </li>
                </ul>
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row-->



</div> <!-- container -->

<script>
function delete_proyek(id_monitoring) {
        // var id_monitoring = $(this).attr('data-id');
        Swal.fire({
          title: 'Apakah anda yakin menghapus data ini ?',
          text: "Anda tidak dapat mengembalikan data yang sudah di hapus !!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proyek/delete_proyek')?>', // replace with your server-side script URL
            data: {id_monitoring:id_monitoring},
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/data_proyek/success_delete";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
          }
        })
    }    
</script>