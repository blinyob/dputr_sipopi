<script src="<?=base_url()?>assets/plugins/dropzone/dropzone.min.js"></script>
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/dropzone/dropzone.min.css" type="text/css" />
                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a>
                                            </li>
                                            <li class="breadcrumb-item"><a
                                                    href="<?=base_url('admin/data_proyek');?>">Proyek</a></li>
                                            <li class="breadcrumb-item active">Detail proyek</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Detail Proyek</h4>
                                    <!-- <a href="javascript:void(0)" class="btn btn-success"><i class="fa fa-plus"></i>Tambah Progress</a> -->
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        <div class="row mb-2"> 
                            <?php if($this->session->tipe=='administrator'):?>
                            <div class="col-sm-4">
                                <button class="btn btn-danger btn-rounded mb-3" id="btn-tambah_detail" data-toggle="modal" data-target="#modal_detail">
                                        <i class="mdi mdi-plus"></i> Tambah Progress
                                </button>
                            </div>
                            <div class="col-sm-8">
<!--                                 <div class="text-sm-right">
                                    <div class="btn-group mb-3">
                                        <a href="<?=base_url('admin/data_proyek');?>" class="btn btn-primary">Semua</a>
                                    </div>
                                    <div class="btn-group mb-3 ml-1">
                                        <a href="<?=base_url('admin/data_proyek_proses');?>" class="btn btn-light">Proses</a>
                                        <a href="<?=base_url('admin/data_proyek_selesai');?>" class="btn btn-light">Selesai</a>
                                    </div>
                                     <div class="btn-group mb-3 ml-2 d-none d-sm-inline-block">
                                        <button type="button" class="btn btn-dark"><i class="dripicons-view-apps"></i></button>
                                    </div>
                                    <div class="btn-group mb-3 d-none d-sm-inline-block">
                                        <button type="button" class="btn btn-link text-dark"><i class="dripicons-checklist"></i></button>
                                    </div> -->
                                </div>
                            </div>
                            <?php  endif; ?>
                            <!-- end col-->
                        </div>

                        <?php if($this->session->flashdata('success')):?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                            role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?= $this->session->flashdata('success');?>
                        </div>
                        <?php endif;?>
                        <?php if($this->session->flashdata('error')):?>
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                            role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?= $this->session->flashdata('error');?>
                        </div>
                        <?php endif;?>
                        <div class="row">
                            <div class="col-md-8">
                                <!-- project card -->
                                <div class="card d-block">
                                    <div class="card-body">
                                        <!-- project title-->
                                        <h3 class="mt-0">
                                            <?= $proyekById['nomor_spk']." - ".date('d F Y',strtotime($proyekById['tanggal_spk']));?>
                                        </h3>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="mb-4">
                                                    <h5>Lokasi</h5>
                                                    <p><?= $proyekById['lokasi'];?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-4">
                                                    <h5>Ruas Jalan</h5>
                                                    <p><?= $proyekById['ruas_jalan'];?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-4">
                                                    <h5>STA</h5>
                                                    <p><?= $proyekById['STA'];?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>Target Penangangan</h5>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p>P = <?= $proyekById['panjang_ruas_jalan'];?> KM</p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p>L = <?= $proyekById['lebar_ruas_jalan'];?> KM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>Jenis Kontruksi</h5>
                                                    <p><?= $proyekById['jenis_kontruksi'];?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>Nilai Kontrak</h5>
                                                    <p><?= 'Rp. '.number_format($proyekById['pagu_anggaran'],2,',','.');?></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>Waktu Pelaksanaan</h5>
                                                    <p><?= $proyekById['waktu_pelaksanaan'];?> Hari
                                                    </p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>SPMK Awal</h5>
                                                    <p><?= date('d F Y', strtotime($proyekById['tanggal_mulai']));?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>SPMK Akhir</h5>
                                                    <p><?=  date('d F Y', strtotime($proyekById['tanggal_akhir']));?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>PHO</h5>
                                                    <p><?=  date('d F Y', strtotime($proyekById['PHO']));?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mb-4">
                                                    <h5>FHO</h5>
                                                    <p><?=  date('d F Y', strtotime($proyekById['FHO']));?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="row">
                                            <div class="col-md-8">
                                                <h5>Penanggung Jawab</h5>
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                                    title="" data-original-title="Penanggung Jawab"
                                                    class="d-inline-block">
                                                    <h4><?= $proyekById['pengawas'];?></h4>
                                                </a>
                                            </div>
                                            <div class="col-md-4">
                                                <h5>Kontraktor</h5>
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                                    title="" data-original-title="Kontraktor" class="d-inline-block">
                                                    <h4><?= $proyekById['penyedia_jasa'];?></h4>
                                                </a>
                                            </div>
                                        </div>

                                    </div> <!-- end card-body-->

                                </div> <!-- end card-->


                            </div> <!-- end col -->

                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title mb-3">Total Progress</h5>
                                        <div class="mt-3">
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar"
                                                    aria-valuenow="<?= $proyekById['persentase'];?>"
                                                    aria-valuemin="0" aria-valuemax="100"
                                                    style="width: <?= $proyekById['persentase'];?>%;">
                                                </div><!-- /.progress-bar -->
                                            </div><!-- /.progress -->
                                            <h1><?= number_format($proyekById['persentase'],0,'.',',');?>%</h1>
                                        </div>
                                        <hr style="border-top:1px solid #0000ff;">
                                        <h5 class="card-title mb-3">Rencana</h5>
                                        <div class="mt-3">
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar"
                                                    aria-valuenow="<?= $proyekById['rencana'];?>" aria-valuemin="0"
                                                    aria-valuemax="100" style="width: <?= $proyekById['rencana'];?>%;">
                                                </div><!-- /.progress-bar -->
                                            </div><!-- /.progress -->
                                            <h5><?= number_format($proyekById['rencana'],0,'.',',');?>%</h5>
                                        </div>


                                        <h5 class="card-title mb-3">Realisasi</h5>
                                        <div class="mt-3">
                                            <div class="progress progress-sm">
                                                <div class="progress-bar" role="progressbar"
                                                    aria-valuenow="<?= $proyekById['persentase'];?>" aria-valuemin="0"
                                                    aria-valuemax="100"
                                                    style="width: <?= $proyekById['persentase'];?>%;">
                                                </div><!-- /.progress-bar -->
                                            </div><!-- /.progress -->
                                            <h5><?= number_format($proyekById['persentase'],0,'.',',');?>%</h5>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class=" card">
                                    <div class="row">
                                        <?php if ($proyekById['laporan_ke'] == NULL) { ?>
                                            <h3>Belum Ada Progress</h3>
                                        <?php }else{ ?>
                                            <?php foreach ($proyekDetail->result() as $row) { ?>
                                            <!-- Gallery item -->
                                            <div class="col-xl-3 col-lg-4 col-md-6 mb-4">
                                            <div class="bg-white rounded shadow-sm">
                                                    <!-- <img src="<?=base_url().$row->file?>" alt="" class="img-fluid card-img-top"> -->
                                              <div class="p-4">
                                                <h5> <a href="#" class="text-dark">Laporan Ke - <?=$row->laporan_ke?> / Progress : <?=$row->persentase_detail?>%</a></h5>
                                                <p class="small mb-0">Rincian : </p>
                                                <p class="small text-muted mb-0"><?=$row->keterangan_detail?></p>
                                                <br><br><br>
                                                <p class="small text-muted mb-0"><b>Pemeriksa : <?=$row->pemeriksa?></b></p>
                                                        <div class="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2">
                                                            <a href="#modal_list_file" class="list_file" data-toggle="modal" data-id="<?=$row->id_monitoring_trMonitoring?>" data-laporan=<?=$row->laporan_ke?>>
                                                                  <p class="small mb-0"><i class="fa fa-picture-o mr-2"></i><span class="font-weight-bold">List File</span></p>
                                                            </a>
                                                            <a href="#modal_edit" data-toggle="modal" class="edit-detail" data-id="<?=$row->id_monitoring?>" data-laporan="<?=$row->laporan_ke?>">
                                                                <div class="badge badge-warning px-2 rounded-pill font-weight-normal">Edit</div>
                                                            </a>
                                                            <a href="#" class="delete-detail" data-id="<?=$row->id_monitoring?>" data-laporan="<?=$row->laporan_ke?>">
                                                              <div class="badge badge-danger px-2 rounded-pill font-weight-normal">delete</div>
                                                            </a>
                                                        </div>
                                              </div>
                                            </div>
                                            </div>
                                            <!-- End -->
                                            <?php } ?>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->


<div id="modal_detail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left">Tambah Progres Proyek</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url().'Proyek/simpan_detail'?>" id="upload-form" class="dropzone">
          <!-- this is were the previews should be shown. -->
          <div class="previews"></div>
          
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="nomor" class="col-form-label">Pemeriksa</label>
                    <input type="text" class="form-control" name="PIC" id="pemeriksa" placeholder="Pemeriksa Proyek Ke Lokasi" required>
                    <input type="hidden" name="id_monitoring" value="<?=$id_monitoring?>">
                </div>
                <div class="form-group col-md-6">
                    <label for="nomor" class="col-form-label">Persentase</label>
                    <input type="number" class="form-control" name="persentase" id="persentase" placeholder="Persentase Proyek (%)" min="<?=$proyekById['persentase']?>" max="<?=$proyekById['rencana']?>" required>
                    <input type="hidden" id="persentase_rencana" value="<?=$proyekById['rencana']?>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="check1" name="status" value="close">
                      <label class="form-check-label">Close Project</label>
                    </div>

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="nomor" class="col-form-label">Rincian Update</label>
                    <textarea  class="form-control" name="rincian" id="rincian" placeholder="Rincian Update Proyek" required></textarea>
                    <!-- <input type="text" class="form-control" placeholder="Pagu Anggaran" required> -->
                </div>
            </div>

          <!-- <button type="submit">Submit data and files!</button> -->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-input">Simpan</button>
      </div>
    </div>

  </div>
</div>


<div id="modal_edit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left">Edit Progres Proyek</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="edit_detail-form" >
          <!-- this is were the previews should be shown. -->
          <div class="previews"></div>
          
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="nomor" class="col-form-label">Pemeriksa</label>
                    <input type="text" class="form-control" name="PIC" id="pemeriksa_detail" placeholder="Pemeriksa Proyek Ke Lokasi" required>
                    <input type="hidden" name="id_monitoring" value="<?=$id_monitoring?>">
                    <input type="hidden" name="laporan" id="laporan" value="">
                </div>
                <div class="form-group col-md-6">
                    <label for="nomor" class="col-form-label">Persentase</label>
                    <input type="number" class="form-control" name="persentase" id="persentase_detail" placeholder="Persentase Proyek (%)" min="<?=$proyekById['persentase']?>" max="<?=$proyekById['rencana']?>" required>
                    <input type="hidden" id="persentase_rencana_detail" value="<?=$proyekById['rencana']?>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="check1_detail" name="status" value="close">
                      <label class="form-check-label">Close Project</label>
                    </div>

                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="nomor" class="col-form-label">Rincian Update</label>
                    <textarea  class="form-control" name="rincian" id="rincian_detail" placeholder="Rincian Update Proyek" required></textarea>
                    <!-- <input type="text" class="form-control" placeholder="Pagu Anggaran" required> -->
                </div>
            </div>

          <!-- <button type="submit">Submit data and files!</button> -->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-warning" id="btn-edit">Edit</button>
      </div>
    </div>

  </div>
</div>


<div id="modal_list_file" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left" id="judul_list_file">List File</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <p id="file_download"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script src="<?=base_url();?>assets/js/jquery/jquery.js"></script>
<script type="text/javascript">
function loading() {
    Swal.fire({
      // title: 'Sweet!',
      // text: 'Modal with a custom image.',
      imageUrl: '<?=base_url();?>assets/images/loading.gif',
      showConfirmButton: false,
      imageWidth: 200,
      imageHeight: 200,
      // imageAlt: 'Custom image',
    })
}    
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#btn-tambah_detail').on('click',function() {
        $('#btn-edit').hide();
        $('#btn-input').show();
        $('#pemeriksa').val('');
        $('#persentase').val('');
        $('#rincian').val('');
        $('#check1').prop('checked',false);
    })

    $('#check1').on('change',function() {
        var persentase          = $('#persentase').val();
        var persentase_rencana  = $('#persentase_rencana').val();
        Swal.fire({
          title: 'Apakah Anda Yakin Untuk Close Project ?',
          showCancelButton: true,
          confirmButtonText: 'Ya',
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            if (parseInt(persentase) < parseInt(persentase_rencana)) {
                console.log(persentase,persentase_rencana)
                Swal.fire('Persentase belum mencapai ketentuan Perencanaan', '', 'error');
                $('#check1').prop('checked',false);
            }
            // Swal.fire('Saved!', '', 'success')
          // } else if (result.isDenied) {
          //   Swal.fire('Changes are not saved', '', 'info')
          }
        })
    });

    $('.list_file').on('click',function() {
        var id_monitoring   = $(this).attr('data-id');
        var laporan         = $(this).attr('data-laporan');
        var html = '';
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/proyek/get_proyek_detail')?>', // replace with your server-side script URL
        data: {id_monitoring:id_monitoring, laporan:laporan},
        encode: true,
          success: function(result){
            var row = JSON.parse(result);
            html += '<div class="row">'
            for (var i = 0; i < row.length; i++) {
                console.log(row[i])
                html += '<div class="col-md-6">';
                html += '   <div class="card mb-1 shadow-none border border-light">';
                html += '       <div class="p-2">';
                html += '           <div class="row align-items-center">';
                html += '               <div class="col-auto">';

                if (row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'jpeg' || row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'jpg' || row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'PNG' || row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'gif'  || row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'png') {
                    html += '                   <img src="<?=base_url();?>' +row[i].lampiran+ '"class="avatar-sm rounded" alt="file" />'
                }else if (row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'pdf') {
                    html +=                     '<span class="avatar-title rounded">PDF</span>'
                }else if (row[i].lampiran.substr((row[i].lampiran.lastIndexOf('.') +1)) == 'xlsx') {
                    html +=                     '<span class="avatar-title rounded">XLSX</i></span>'
                }

                html += '               </div>';
                html += '               <div class="col pl-0">'
                html += '                   <a href="javascript:void(0);" class="text-muted font-weight-bold">'+row[i].lampiran.substr(19)+'</a>';
                html += '               </div>'
                html += '               <div class="col-auto">';
                html += '                   <a href="<?=base_url();?>'+row[i].lampiran+'" class="btn btn-link btn-lg text-muted" target="_blank">'
                html += '                       <i class="dripicons-download"></i>'
                html += '                   </a>'
                html += '               </div>'
                html += '           </div>';
                html += '       </div>';
                html += '   </div>';
                html += '</div>';
            }
            html += '</div">'
            console.log(html)
            $('#file_download').html(html);

            // var data = JSON.parse(result);
            // $('#pagu_anggaran').val(formatRupiah(data[0].pagu));
            // $('#anggaran').val(data[0].pagu);
            // $('#kode_program').val(data[0].kode_program);
            // $('#kode_kegiatan').val(data[0].kode_kegiatan);
            // $('#kode_sub_kegiatan').val(data[0].kode_sub_kegiatan);
          }
        });
    });

    $('.edit-detail').on('click',function() {
        $('#btn-edit').show();
        $('#btn-input').hide();
        var id_monitoring = $(this).attr("data-id");
        var laporan = $(this).attr("data-laporan");
          $.ajax({
          type: 'POST',
          url: '<?=base_url('index.php/proyek/get_proyek_detail')?>', // replace with your server-side script URL
          data: {id_monitoring:id_monitoring, laporan:laporan},
          encode: true,
            success: function(result){
            var data = JSON.parse(result);
            console.log(data)
                $('#pemeriksa_detail').val(data[0].pemeriksa);
                $('#persentase_detail').val(data[0].persentase_detail);
                $('#rincian_detail').val(data[0].keterangan_detail);
                $('#laporan').val(data[0].laporan_ke);
            }
         })
    })

    $('#check1_detail').on('change',function() {
        var persentase          = $('#persentase_detail').val();
        var persentase_rencana  = $('#persentase_rencana_detail').val();
        Swal.fire({
          title: 'Apakah Anda Yakin Untuk Close Project ?',
          showCancelButton: true,
          confirmButtonText: 'Ya',
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            if (parseInt(persentase) < parseInt(persentase_rencana)) {
                console.log(persentase,persentase_rencana)
                Swal.fire('Persentase belum mencapai ketentuan Perencanaan', '', 'error');
                $('#check1_detail').prop('checked',false);
            }
            // Swal.fire('Saved!', '', 'success')
          // } else if (result.isDenied) {
          //   Swal.fire('Changes are not saved', '', 'info')
          }
        })
    });

    $("#btn-edit").on("click", function(e) {
        var data = $('#edit_detail-form').serialize();
        var pemeriksa   = $('#pemeriksa_detail').val();
        var persentase  = $('#persentase_detail').val();
        var rincian     = $('#rincian_detail').val();
        loading();
        if (pemeriksa == "") {
            Swal.fire('PERINGATAN !!', 'Pemeriksa Wajib Di Isi', 'warning');
        }else if (persentase == "") {
            Swal.fire('PERINGATAN !!', 'Persentase Wajib Di Isi', 'warning');
        }else if (parseInt(persentase) > parseInt("<?=$proyekById['rencana']?>")) {
            Swal.fire('PERINGATAN !!', 'Persentase  Melebihi Perencanaan', 'warning');
            $('#persentase').val('');
        // }else if (parseInt(persentase) < parseInt("<?=$proyekById['persentase']?>")) {
        //     Swal.fire('PERINGATAN !!', 'Persentase  Tidak Boleh Kurang Dari Progres Sebelumnya', 'warning');
            $('#persentase').val('');
        }else if (rincian == "") {
            Swal.fire('PERINGATAN !!', 'Rincian Update Proyek Wajib Di Isi', 'warning');
        }else{
            // Make sure that the form isn't actually being sent.
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proyek/edit_detail')?>', // replace with your server-side script URL
            data: data,
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/detail_proyek/"+<?=$proyekById['id_monitoring']?>+"/success_edit";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
        }
    });

    $('.delete-detail').on('click',function() {
        var id_monitoring = $(this).attr('data-id');
        var laporan = $(this).attr('data-laporan');
        Swal.fire({
          title: 'Apakah anda yakin menghapus data ini ?',
          text: "Anda tidak dapat mengembalikan data yang sudah di hapus !!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proyek/delete_detail')?>', // replace with your server-side script URL
            data: {id_monitoring:id_monitoring, laporan:laporan},
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/detail_proyek/"+<?=$proyekById['id_monitoring']?>+"/success_delete";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
          }
        })
    })

    // $('#btn-edit').on('click',function() {
    //     var data = $('#form-proyek').serialize();
    //     loading();
    //     $.ajax({
    //     type: 'POST',
    //     url: '<?=base_url('index.php/Proyek/edit_detail')?>', // replace with your server-side script URL
    //     data: data,
    //     encode: true,
    //       success: function(result){
    //         console.log(result)
    //         if (result == "Berhasil") {
    //             Swal.fire({
    //                       title: "SUCCESS",
    //                       text: "Data Berhasil Di Simpan",
    //                       icon: "success",
    //                       showConfirmButton: false,
    //                       timer: 3000,
    //                     }).then(() => {
    //               window.location.href="<?=base_url()?>admin/data_proyek/success";
    //             });
    //         }else if (result == "user sudah ada") {
    //             Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
    //         }else{
    //             Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
    //         }
    //       }
    //     });
    // })

});
</script>
<script>
Dropzone.options.uploadForm = { // The camelized version of the ID of the form element

  // The configuration we've talked about above
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFiles: 100,

  // The setting up of the dropzone
  init: function() {
    var myDropzone = this;

    // First change the button to actually tell Dropzone to process the queue.
    $("#btn-input").on("click", function(e) {
        var pemeriksa   = $('#pemeriksa').val();
        var persentase  = $('#persentase').val();
        var rincian     = $('#rincian').val();
        if (pemeriksa == "") {
            Swal.fire('PERINGATAN !!', 'Pemeriksa Wajib Di Isi', 'warning');
        }else if (persentase == "") {
            Swal.fire('PERINGATAN !!', 'Persentase Wajib Di Isi', 'warning');
        }else if (parseInt(persentase) > parseInt("<?=$proyekById['rencana']?>")) {
            Swal.fire('PERINGATAN !!', 'Persentase  Melebihi Perencanaan', 'warning');
            $('#persentase').val('');
        }else if (parseInt(persentase) < parseInt("<?=$proyekById['persentase']?>")) {
            Swal.fire('PERINGATAN !!', 'Persentase  Tidak Boleh Kurang Dari Progres Sebelumnya', 'warning');
            $('#persentase').val('');
        }else if (rincian == "") {
            Swal.fire('PERINGATAN !!', 'Rincian Update Proyek Wajib Di Isi', 'warning');
        }else{
            // Make sure that the form isn't actually being sent.
            e.preventDefault();
            e.stopPropagation();
            myDropzone.processQueue();
        }
    });


    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
    // of the sending event because uploadMultiple is set to true.
    this.on("sendingmultiple", function() {
        loading();
      // Gets triggered when the form is actually being sent.
      // Hide the success button or the complete form.
    });
    this.on("successmultiple", function(files, response) {
        if (response == "Berhasil") {
            Swal.fire({
                      title: "SUCCESS",
                      text: "Data Berhasil Di Simpan",
                      icon: "success",
                      showConfirmButton: false,
                      timer: 3000,
                    }).then(() => {
              window.location.href="<?=base_url()?>admin/detail_proyek/"+<?=$id_monitoring?>+"/success";
            });
        }else{
            Swal.fire('Whoops, Somthing is Wrong!', response, 'warning')
        }
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
    });
    this.on("errormultiple", function(files, response) {
        console.log("Error");
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    });
  }
 
}
</script>