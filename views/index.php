<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from coderthemes.com/hyper/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Aug 2018 21:45:59 GMT -->

<head>
    <meta charset="utf-8" />
    <title>SIPOPI | Halaman admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/pu.png">

    <!-- third party css -->
    <link href="<?=base_url();?>assets/css/vendor/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>assets/css/vendor/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- <link href="<?=base_url();?>assets/css/vendor/responsive.bootstrap4.css" rel="stylesheet" type="text/css" /> -->
    <link href="<?=base_url();?>assets/css/vendor/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>assets/css/vendor/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>assets/plugins/switch.css" rel="stylesheet" type="text/css" />
    <!-- <link href="<?=base_url();?>assets/vendor/select2/css/select2.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="<?=base_url();?>assets/plugins/select2.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

    <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/sweetalert/sweetalert2.css" />    
    <!-- third party css end -->

    <!-- App css -->
    <link href="<?=base_url();?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        @media print {
            table, table tr, table th, table td {
                border-width:1px;
                border-style:solid;
                border-color:black;
            }
        }
/*        @media print {
            table, table tr, table th, table td {
                border-top: #000 solid 1px;
                border-bottom: #000 solid 1px;
                border-left: #000 solid 1px;
                border-right: #000 solid 1px;
            }
        } */
    </style>

</head>

<body class="enlarged" data-keep-enlarged="true">

    <!-- Begin page -->
    <div class="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">

            <div class="slimscroll-menu">

                <!-- LOGO -->
                <a href="<?=base_url('admin');?>" class="logo text-center">
                    <span class="logo-lg">
                        <img src="<?=base_url();?>assets/images/pu.png" alt="" height="16">
                    </span>
                    <span class="logo-sm">
                        <img src="<?=base_url();?>assets/images/pu.png" alt="" height="16">
                    </span>
                </a>

                <!--- Sidemenu -->
                <ul class="metismenu side-nav">

                    <li class="side-nav-title side-nav-item">Daftar Menu | Repost by <a href='https://stokcoding.com/' style="color: white;" title='StokCoding.com' target='_blank'>StokCoding.com</a>
					</li>

                    <li class="side-nav-item">
                        <a href="<?=base_url('admin');?>" class="side-nav-link">
                            <i class="dripicons-meter"></i>
                            <span> Beranda </span>
                        </a>
                    </li>
                    <li class="side-nav-item">
                        <a href="<?=base_url('admin/aknop');?>" class="side-nav-link">
                            <i class="dripicons-list"></i>
                            <!-- <span class="badge badge-success float-right">7</span> -->
                            <span> Aknop </span>
                        </a>
                    </li>
                    <?php if($this->session->tipe=='administrator'):?>
                    <li class="side-nav-item">
                        <a href="<?=base_url('admin/irigasi');?>" class="side-nav-link">
                            <i class="dripicons-pamphlet"></i>
                            <!-- <span class="badge badge-success float-right">7</span> -->
                            <span> Data Irigasi </span>
                        </a>
                    </li>
                    <li class="side-nav-item">
                        <a href="<?=base_url('admin/pengguna');?>" class="side-nav-link">
                            <i class="dripicons-user"></i>
                            <!-- <span class="badge badge-success float-right">7</span> -->
                            <span> Pengguna </span>
                        </a>
                    </li>
<!-- 
                    <li class="side-nav-item">
                        <a href="#modal-report" data-toggle="modal" class="side-nav-link">
                            <i class="mdi mdi-file"></i>
                             -- <span class="badge badge-success float-right">7</span> --
                            <span> Report </span>
                        </a>
                    </li>
 -->
                    <?php endif; ?>
                    <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Topbar Start -->
                <div class="navbar-custom">
                    <ul class="list-unstyled topbar-right-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" href="#"
                                role="button" aria-haspopup="false" aria-expanded="false">
                                <span class="account-user-avatar">
                                    <img src="<?=base_url();?>assets/images/pu.png" alt="user-image"
                                        class="rounded-circle">
                                </span>
                                <span>
                                    <span class="account-user-name"><?=$this->session->userdata('fullname');?></span>
                                    <span class="account-position"><?=$this->session->userdata('tipe');?></span>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                <!-- item-->
                                <div class=" dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Welcome !</h6>
                                </div>



                                <!-- item-->
                                <a href="<?=base_url('admin/logout');?>" class="dropdown-item notify-item">
                                    <i class="mdi mdi-logout"></i>
                                    <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>
                    <button class="button-menu-mobile open-left disable-btn">
                        <i class="mdi mdi-menu"></i>
                    </button>
                    <!-- <div class="app-search">
						<form>
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search...">
								<span class="mdi mdi-magnify"></span>
								<div class="input-group-append">
									<button class="btn btn-primary" type="submit">Search</button>
								</div>
							</div>
						</form>
					</div> -->
                </div>
                <!-- end Topbar -->

                <?php $this->load->view($content);?>

            </div>
            <!-- content -->

            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <?= date('Y'); ?> ©  <a href='#' title='StokCoding.com' target='_blank'>SIPOPI</a>
							
                        </div>
                        <div class="col-md-6">
                            <div class="text-md-right footer-links d-none d-md-block">
                                <!-- <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


<div id="modal-report" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left">Report</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?=base_url().'Proyek/report_pdf'?>" method="post" target="_blank">
          <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-12 row">
                        <label for="nomor" class="col-form-label col-md-4">Bulan</label>
                        <div class="col-md-8">
                            <select class="form-control" name="bulan">
                                <option value=""></option>
                                <option value="1">Januari</option>
                                <option value="2">Februari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                    </div>
                </div>
              <!-- <button type="submit">Submit data and files!</button> -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success" id="btn-input">Export</button>
          </div>
      </form>
    </div>

  </div>
</div>


    <!-- bundle -->
    <script src="<?=base_url();?>assets/js/app.min.js"></script>

    <!-- third party js -->
    <script src="<?=base_url();?>assets/js/vendor/Chart.bundle.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/jquery.dataTables.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/dataTables.bootstrap4.js"></script>
    <!-- <script src="<?=base_url();?>assets/js/vendor/dataTables.responsive.min.js"></script> -->
    <script src="<?=base_url();?>assets/js/vendor/responsive.bootstrap4.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/dataTables.buttons.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/buttons.bootstrap4.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/buttons.html5.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/buttons.flash.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/buttons.print.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url();?>assets/js/vendor/dataTables.select.min.js"></script>

    <!-- <script src="<?=base_url();?>assets/vendor/select2/js/select2.min.js"></script> -->
    <script src="<?=base_url()?>assets/js/vendor/sweetalert/sweetalert2.js"></script>
    <!-- third party js ends -->

    <!-- demo app -->
    <script src="<?=base_url();?>assets/js/pages/demo.dashboard.js"></script>
    <script src="<?=base_url();?>assets/js/pages/demo.datatable-init.js"></script>
    <!-- end demo js-->

    <script type="text/javascript">
          var options = {
            dom: '<"dataTables_wrapper dt-bootstrap"<"row"<"col-xl-7 d-block d-sm-flex d-xl-block justify-content-center"<"d-block d-lg-inline-flex me-0 me-md-3"l><"d-block d-lg-inline-flex"B>><"col-xl-5 d-flex d-xl-block justify-content-center"fr>>t<"row"<"col-md-5"i><"col-md-7"p>>>',
            buttons: [
              { extend: 'copy', className: 'btn-sm' },
              { extend: 'csv', className: 'btn-sm' },
              { extend: 'excel', className: 'btn-sm' },
              { extend: 'pdf', className: 'btn-sm' },
              { extend: 'print', className: 'btn-sm' }
            ],
            responsive: false,
            // colReorder: true,
            // keys: true,
            // order: [[1, 'asc']],
            // rowReorder: true,
          };

          if ($(window).width() <= 767) {
            options.rowReorder = false;
            options.colReorder = false;
          }
          $('#data_lengkap').DataTable(options);

    </script>
</body>


<!-- Mirrored from coderthemes.com/hyper/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Aug 2018 21:46:28 GMT -->

</html>
