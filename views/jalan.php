<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a></li>

                        <li class="breadcrumb-item active">Jalan</li>
                    </ol>
                </div>
                <h4 class="page-title">Data Jalan</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row mb-2">
        <div class="col-sm-4">
            <a href="#modal-jalan" data-toggle="modal" class="btn btn-danger btn-rounded mb-3" id="btn-tambah"><i
                    class="mdi mdi-plus"></i> Tambah
                Data Jalan</a>
        </div>
        <div class="col-sm-8">
            <div class="text-sm-right">
            </div>
        </div><!-- end col-->
    </div>
    <!-- end row-->

    <div class="row">
        <div class="col-12">
            <?php if($this->session->flashdata('success')):?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $this->session->flashdata('success');?>
            </div>
            <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama Jalan</th>
                                <th>Keterangan</th>
                                <th>Tanggal Input</th>
                                <th>Last Update</th>
                                <th width="100">Aksi</th>
                            </tr>
                        </thead>


                        <tbody>
                            <?php $n=1; foreach($jalan as $row):?>
                            <tr>
                                <td><?php echo $n++;?></td>
                                <td><?php echo $row['nama_jalan'];?></td>                                
                                <td><?php echo $row['keterangan'];?></td>                                
                                <td><?php echo $row['dtmDateInserted'];?></td>                                
                                <td><?php echo $row['dtmDateUpdated'];?></td>                                
                                <td>
                                     <a href="#modal-jalan" data-id="<?=$row['id_jalan']?>"
                                        class="btn btn-icon btn-info btn-sm edit_jalan" data-toggle="modal" data-placement="top"
                                        title="" data-original-title="Edit"> <i class="mdi mdi-account-edit"></i> </a>
                             
                                    <a href="#" data-id="<?=$row['id_jalan']?>" data-jalan="<?=$row['nama_jalan']?>"
                                        class="btn btn-icon btn-danger btn-sm delete-jalan" data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title="Hapus"> 
                                        <i class="mdi mdi-account-remove"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach;?>

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

<div id="modal-jalan" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-left">Tambah Data Jalan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="form-jalan">
          <!-- this is were the previews should be shown. -->
          <div class="previews"></div>
          
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="nomor" class="col-form-label">Nama jalan</label>
                    <input type="text" class="form-control" name="nama_jalan" id="nama_jalan" placeholder="Masukan Nama Jalan" required>
                    <input type="hidden" name="id_jalan" id="id_jalan" value="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="nomor" class="col-form-label">Keterangan</label>
                    <textarea  class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan Jalan" required></textarea>
                    <!-- <input type="text" class="form-control" placeholder="Pagu Anggaran" required> -->
                </div>
            </div>

          <!-- <button type="submit">Submit data and files!</button> -->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btn-input">Simpan</button>
        <button type="button" class="btn btn-warning" id="btn-edit">Edit</button>
      </div>
    </div>

  </div>
</div>






</div> <!-- container -->
<script src="<?=base_url();?>assets/js/jquery/jquery.js"></script>
<script type="text/javascript">
function loading() {
    Swal.fire({
      // title: 'Sweet!',
      // text: 'Modal with a custom image.',
      imageUrl: '<?=base_url();?>assets/images/loading.gif',
      showConfirmButton: false,
      imageWidth: 200,
      imageHeight: 200,
      // imageAlt: 'Custom image',
    })
}    
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#btn-tambah').on('click',function() {
        $('#btn-edit').hide();
        $('#btn-input').show();
        $('#nama_jalan').val('');
        $('#keterangan').val('');
    })

    $("#btn-input").on("click", function() {
        var data = $('#form-jalan').serialize();
        var nama_jalan   = $('#nama_jalan').val();
        loading();
        if (nama_jalan == "") {
            Swal.fire('PERINGATAN !!', 'Nama Jalan Wajib Di Isi', 'warning');
        }else{
            // Make sure that the form isn't actually being sent.
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proses/simpan_jalan')?>', // replace with your server-side script URL
            data: data,
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/jalan";
                    });
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
        }
    });

    $('.edit_jalan').on('click',function() {
        var id_jalan = $(this).attr('data-id');
        console.log(id_jalan)
        $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/proses/get_jalan')?>', // replace with your server-side script URL
        data: {id_jalan:id_jalan},
        encode: true,
            success: function(result){
            var data = JSON.parse(result);
            console.log(data)
                $('#btn-input').hide();
                $('#btn-edit').show();
                $('#nama_jalan').val(data[0].nama_jalan);
                $('#keterangan').val(data[0].keterangan);
                $('#id_jalan').val(data[0].id_jalan);
            }
        })
    })

    $("#btn-edit").on("click", function(e) {
        var data = $('#form-jalan').serialize();
        var nama_jalan   = $('#nama_jalan').val();
        loading();
        if (nama_jalan == "") {
            Swal.fire('PERINGATAN !!', 'Nama Jalan Wajib Di Isi', 'warning');
        }else{
            // Make sure that the form isn't actually being sent.
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proses/edit_jalan')?>', // replace with your server-side script URL
            data: data,
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/jalan";
                    });
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
        }
    });

    $('.delete-jalan').on('click',function() {
        var id_jalan = $(this).attr('data-id');
        var nama_jalan = $(this).attr('data-jalan');
        Swal.fire({
          title: 'Apakah anda yakin menghapus Jalan '+nama_jalan+' ?',
          text: "Anda tidak dapat mengembalikan data yang sudah di hapus !!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proses/delete_jalan')?>', // replace with your server-side script URL
            data: {id_jalan:id_jalan},
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/jalan/success_delete";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
          }
        })
    })
});    
</script>