<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url('admin/data_proyek');?>">Proyek</a></li>
                        <li class="breadcrumb-item active">Tambah Proyek</li>
                    </ol>
                </div>
                <h4 class="page-title">Form Tambah Proyek</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- Form row -->
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body">

                    <!-- <form action="<?=base_url('proses/edit_proyek');?>" method="post"> -->
                    <form method="post" id="form-proyek">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="namaProyek" class="col-form-label">Nama Kegiatan</label>
                                <select class="form-control select2" data-toggle="select2" id="kegiatan" name="id_sub_kegiatan" required>
                                    <option>Pilih Kegiatan</option>
                                    <?php foreach ($kegiatan->result() as $row) { ?>
                                        <option value="<?=$row->id_sub_kegiatan?>" <?php if($proyekById['id_sub_kegiatan_dtSubKegiatan'] == $row->id_sub_kegiatan){ echo "selected";}?> ><?=$row->kode_program.".".$row->kode_kegiatan.".".$row->kode_sub_kegiatan.".".$row->nama_sub_kegiatan?></option>
                                    <?php } ?>

                                </select>
                                <!-- <input type="text" class="form-control" id="nama_proyek" name="nama" -->
                                    <!-- placeholder="Masukkan nama proyek"> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nomor" class="col-form-label">Anggaran</label>
                                <input type="text" class="form-control" id="pagu_anggaran" 
                                    placeholder="Pagu Anggaran" value="<?="Rp " . number_format($proyekById['pagu_anggaran'],2,',','.')?>" required>

                                <input type="hidden" name="kode_program" id="kode_program" value="<?=$proyekById['kode_program']?>">
                                <input type="hidden" name="kode_kegiatan" id="kode_kegiatan" value="<?=$proyekById['kode_kegiatan']?>">
                                <input type="hidden" name="kode_sub_kegiatan" id="kode_sub_kegiatan" value="<?=$proyekById['kode_sub_kegiatan']?>">
                                <input type="hidden" name="anggaran" id="anggaran" value="<?=$proyekById['pagu_anggaran']?>">
                                <input type="hidden" name="id_monitoring" id="anggaran" value="<?=$proyekById['id_monitoring']?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="nomor" class="col-form-label">Ruas Jalan</label>
                                <select class="form-control select2" data-toggle="select2" name="ruas_jalan" required>
                                    <option value="">Pilih Jalan</option>
                                    <?php foreach ($jalan as $row) { ?>
                                        <option value="<?=$row['nama_jalan']?>" <?php if ($proyekById['ruas_jalan']  == $row['nama_jalan']) { echo "selected"; }?>><?=$row['nama_jalan']?></option>
                                    <?php } ?>
                                </select>
<!--                                 <input type="text" class="form-control" name="ruas_jalan" value="<?=$proyekById['ruas_jalan']?>"
                                    placeholder="Masukkan Nama Ruas Jalan" required> -->
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nomor" class="col-form-label">Lokasi</label>
                                <input type="text" class="form-control" name="lokasi" value="<?=$proyekById['lokasi']?>"
                                    placeholder="Masukkan Lokasi proyek" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nomor" class="col-form-label">STA</label>
                                <input type="text" class="form-control" name="sta" value="<?=$proyekById['STA']?>"
                                    placeholder="Masukkan STA Project" required>
                            </div>


                            <div class="form-group col-md-4 row">
                                <div class="col-md-12 text-center">
                                    <label for="nomor" class="col-form-label">Target Penanganan</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control" name="panjang_ruas_jalan" value="<?=$proyekById['panjang_ruas_jalan']?>"
                                        placeholder="Panjang Ruas Jalan" required>
                                </div>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control" name="lebar_ruas_jalan" value="<?=$proyekById['lebar_ruas_jalan']?>"
                                        placeholder="Lebar Ruas Jalan" required>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nomor" class="col-form-label">Jenis Kontruksi</label>
                                <input type="text" class="form-control" name="jenis_kontruksi" value="<?=$proyekById['jenis_kontruksi']?>"
                                    placeholder="Masukkan Jenis Kontruksi" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nomor" class="col-form-label">Nilai Kontrak</label>
                                <input type="text" class="form-control" name="nilai_kontrak" value="<?=$proyekById['nilai_kontrak']?>"
                                    placeholder="Masukkan Nilai Kontrak" required>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">No.SPK</label>
                                <input type="text" class="form-control" name="nomor_spk" value="<?=$proyekById['nomor_spk']?>"
                                    placeholder="Masukan Nomor SPK" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">Tanggal SPK</label>
                                <input type="date" class="form-control" name="tgl_spk" value="<?=$proyekById['tanggal_spk']?>"
                                    placeholder="Masukkan Tanggal SPK" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">Waktu Pelaksanaan</label>
                                <input type="number" class="form-control" name="waktu_pelaksanaan" value="<?=$proyekById['waktu_pelaksanaan']?>"
                                    placeholder="Masukkan Tanggal Pelaksanaan" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">Rencana</label>
                                <input type="number" class="form-control" name="rencana" value="<?=$proyekById['rencana']?>"
                                    placeholder="Masukkan Persentase Perencanaan" required>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">SPMK Awal</label>
                                <input type="date" class="form-control" name="spmk_awal" value="<?=$proyekById['tanggal_mulai']?>"
                                    placeholder="Masukan Tanggal Awal SPMK" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">SPMK akhir</label>
                                <input type="date" class="form-control" name="spmk_akhir" value="<?=$proyekById['tanggal_akhir']?>"
                                    placeholder="Masukan Tanggal Akhir SPMK" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">Penyedia Jasa</label>
                                <input type="text" class="form-control" name="penyedia_jasa" value="<?=$proyekById['penyedia_jasa']?>"
                                    placeholder="Masukkan Nama Penyedia Jasa" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="nomor" class="col-form-label">Pengawas</label>
                                <input type="text" class="form-control" name="pengawas" value="<?=$proyekById['pengawas']?>"
                                    placeholder="Masukkan Nama Pengawas" required>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="nomor" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" name="keterangan" placeholder="Masukan Keterangan Proyek" name="keterangan" required><?=$proyekById['keterangan']?></textarea>

<!--                                 <input type="text" class="form-control" name="pengawas"
                                    placeholder="Masukkan Nama Pengawas"> -->
                            </div>

                        </div>


                        <a href="<?=base_url('admin/data_proyek');?>" class="btn btn-primary float-left"><i
                                class="mdi mdi-undo"></i> Kembali </a>
                        <button type="button" class="btn btn-success float-right" id="simpan_proyek"><i class="mdi mdi-floppy"></i> Simpan
                        </button>

                    </form>


                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

</div> <!-- container -->

<script src="<?=base_url();?>assets/js/jquery/jquery.js"></script>
<script>
function formatRupiah(amount) {
    var number_string = amount.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    return 'Rp ' + rupiah;
}

function loading() {
    Swal.fire({
      // title: 'Sweet!',
      // text: 'Modal with a custom image.',
      imageUrl: '<?=base_url();?>assets/images/loading.gif',
      showConfirmButton: false,
      imageWidth: 200,
      imageHeight: 200,
      // imageAlt: 'Custom image',
    })
}

$(document).ready(function () {
    // $('#nilai_kontrak').on('keyup',function() {

    // })
    $('#kegiatan').on('change',function() {
    var kegiatan = $(this).val();
      $.ajax({
        type: 'POST',
        url: '<?=base_url('index.php/proyek/get_kegiatan_detail')?>', // replace with your server-side script URL
        data: {kegiatan:kegiatan},
        encode: true,
          success: function(result){
            var data = JSON.parse(result);
            $('#pagu_anggaran').val(formatRupiah(data[0].pagu));
            $('#anggaran').val(data[0].pagu);
            $('#kode_program').val(data[0].kode_program);
            $('#kode_kegiatan').val(data[0].kode_kegiatan);
            $('#kode_sub_kegiatan').val(data[0].kode_sub_kegiatan);
          }
        });
    });

  $('#simpan_proyek').on('click',function() {
    var data = $('#form-proyek').serialize();
    loading();
    $.ajax({
    type: 'POST',
    url: '<?=base_url('index.php/Proyek/edit_proyek')?>', // replace with your server-side script URL
    data: data,
    encode: true,
      success: function(result){
        console.log(result)
        if (result == "Berhasil") {
            Swal.fire({
                      title: "SUCCESS",
                      text: "Data Berhasil Di Simpan",
                      icon: "success",
                      showConfirmButton: false,
                      timer: 3000,
                    }).then(() => {
              window.location.href="<?=base_url()?>admin/data_proyek/success";
            });
        }else if (result == "user sudah ada") {
            Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
        }else{
            Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
        }
      }
    });
  })

});
</script>
