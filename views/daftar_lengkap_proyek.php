<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Beranda</a></li>
                        
                        <li class="breadcrumb-item active">Proyek</li>
                    </ol>
                </div>
                <h4 class="page-title">Data Proyek</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row mb-2"> 
        <?php if($this->session->tipe=='administrator'):?>
        <div class="col-sm-4">
            <a href="<?= base_url('admin/tambah_data_proyek');?>" class="btn btn-danger btn-rounded mb-3"><i
                    class="mdi mdi-plus"></i> Buat
                Proyek</a>
            <a href="<?= base_url('admin/data_proyek');?>" class="btn btn-primary btn-rounded mb-3">
                Tampilan sedikit
            </a>
        </div>
        <div class="col-sm-8">
            <div class="text-sm-right">
                <div class="btn-group mb-3">
                    <a href="<?=base_url('admin/data_proyek');?>" class="btn btn-primary">Semua</a>
                </div>
                <div class="btn-group mb-3 ml-1">
                    <a href="<?=base_url('admin/data_proyek_proses');?>" class="btn btn-light">Proses</a>
                    <a href="<?=base_url('admin/data_proyek_selesai');?>" class="btn btn-light">Selesai</a>
                </div>
                <!-- <div class="btn-group mb-3 ml-2 d-none d-sm-inline-block">
                    <button type="button" class="btn btn-dark"><i class="dripicons-view-apps"></i></button>
                </div>
                <div class="btn-group mb-3 d-none d-sm-inline-block">
                    <button type="button" class="btn btn-link text-dark"><i class="dripicons-checklist"></i></button>
                </div> -->
            </div>
        </div>
        <?php  endif; ?>
        <!-- end col-->
    </div>
    <!-- end row-->

    <div class="row">

        <div class="col-12">
            <?php if($this->session->flashdata('success')):?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $this->session->flashdata('success');?>
            </div>
        <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">

                        <!-- <table border="1" width="2000px;"> -->
                        <table id="data_lengkap" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 13px;">
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th width="200px" class="text-center" rowspan="2">KODE SUB KEGIATAN / KODE REKENING</th>
                                    <th width="350px" class="text-center" rowspan="2">SUB KEGIATAN</th>
                                    <th width="150px" class="text-center" rowspan="2">PAGU Rp. </th>
                                    <th width="100px" class="text-center" >Lokasi</th>
                                    <th width="300px" class="text-center" colspan="2">Target Penanganan</th>
                                    <th width="" class="text-center" rowspan="2">Jenis Konstruksi Permukaan</th>
                                    <th width="" class="text-center" rowspan="2"> Nilai Kontrak Rp.</th>
                                    <th width="" class="text-center" rowspan="2">Nomor dan Tgl Kontrak</th>
                                    <th width="" class="text-center" rowspan="2">Waktu Pelaksanaan (HK)</th>
                                    <th width="" class="text-center" colspan="2">SPMK</th>
                                    <th width="300px" class="text-center" rowspan="2">Penyedia Jasa</th>
                                    <th width="300px" class="text-center" rowspan="2">Pengawas</th>
                                    <th width="" class="text-center" rowspan="2">Rencana (%)</th>
                                    <th width="" class="text-center" rowspan="2">Realisasi (%)</th>
                                    <th width="" class="text-center" rowspan="2">Selisih (%)</th>
                                    <th width="300px" class="text-center" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Panjang</th>
                                    <th class="text-center">Lebar</th>
                                    <th width="100px" class="text-center">Tanggal Mulai</th>
                                    <th width="100px" class="text-center">Tanggal Akhir</th>                
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($kegiatan->result() as $row) {
                                        $proyek = $this->Kegiatan_M->get_kegiatan_proyek($row->id_sub_kegiatan);
                                        if ($proyek->num_rows() != NULL) {
                                 ?>
                                     <tr>
                                         <td>#</td>
                                         <td><?php echo $row->kode_program.".".$row->kode_kegiatan.".".$row->kode_sub_kegiatan;?></td>
                                         <td><?php echo $row->nama_sub_kegiatan;?></td>
                                         <td class="text-right"><?php echo number_format($row->pagu,0,',','.');?></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                     </tr>
                                    <?php foreach ($proyek->result() as $data) { ?>
                                     <tr>
                                        <td>#</td>
                                        <td></td>
                                        <td><?php echo $data->ruas_jalan;?></td>
                                        <td class="text-right"><?php echo number_format($data->pagu_anggaran,0,',','.');?></td>
                                        <td><?php echo $data->lokasi;?></td>
                                        <td class="text-right"><?php echo $data->panjang_ruas_jalan;?></td>
                                        <td class="text-right"><?php echo $data->lebar_ruas_jalan;?></td>
                                        <td><?php echo $data->jenis_kontruksi;?></td>
                                        <td><?php echo number_format($data->nilai_kontrak,0,',','.');?></td>
                                        <td class="text-center"><?php echo $data->nomor_spk;?><br><?=date('d F Y',strtotime($data->tanggal_spk))?></td>
                                        <td class="text-right"><?php echo $data->waktu_pelaksanaan;?></td>
                                        <td class="text-center"><?php echo date('d-m-Y',strtotime($data->tanggal_mulai));?></td>
                                        <td class="text-center"><?php echo date('d-m-Y',strtotime($data->tanggal_akhir));?></td>
                                        <td><?php echo $data->penyedia_jasa;?></td>
                                        <td><?php echo $data->pengawas;?></td>
                                        <td><?php echo $data->rencana;?></td>
                                        <td><?php echo $data->persentase;?></td>
                                        <td><?php echo $data->persentase-$data->rencana;?></td>
                                        <td><?php echo $data->keterangan;?></td>
                                     </tr>
                                    <?php } ?>
                                     <tr>
                                         <td>#</td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                         <td></td>
                                     </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

    
                    </li>
                </ul>
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row-->



</div> <!-- container -->

<script>
function delete_proyek(id_monitoring) {
        // var id_monitoring = $(this).attr('data-id');
        Swal.fire({
          title: 'Apakah anda yakin menghapus data ini ?',
          text: "Anda tidak dapat mengembalikan data yang sudah di hapus !!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
            type: 'POST',
            url: '<?=base_url('index.php/Proyek/delete_proyek')?>', // replace with your server-side script URL
            data: {id_monitoring:id_monitoring},
            encode: true,
              success: function(result){
                console.log(result)
                if (result == "Berhasil") {
                    Swal.fire({
                              title: "SUCCESS",
                              text: "Data Berhasil Di Simpan",
                              icon: "success",
                              showConfirmButton: false,
                              timer: 3000,
                            }).then(() => {
                      window.location.href="<?=base_url()?>admin/data_proyek/success_delete";
                    });
                }else if (result == "user sudah ada") {
                    Swal.fire('Whoops!', 'Username Sudah Digunakan', 'warning')
                }else{
                    Swal.fire('Whoops, Somthing is Wrong!', result, 'warning')
                }
              }
            });
          }
        })
    }    
</script>