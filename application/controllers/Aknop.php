<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aknop extends CI_Controller {
	function __construct()
	{
		parent:: __construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		$this->load->model('Aknop_m');
		$this->load->model('Login_m');
	}

	// public function index()
	// {
	// 	$data['content'] = 'daftar_aknop';
	// 	$data['aknop'] = $this->Aknop_m->get_proyek();
	// 	$this->load->view('index',$data);
	// }

	public function simpan_aknop()
	{
		$post = $this->input->post();
		if ($post['nama_irigasi'] == "") {
			echo "<h2 style='color: red'>Nama Irigasi Belum Di Isi</h2>";
		}else if ($post['tahun_anggaran'] == "") {
			echo "<h2 style='color: red'>Tahun Anggaran Belum Di Isi</h2>";
		}else if($post['prasarana_fisik'] == ""){
			echo "<h2 style='color: red'>Nilai Prasaranan Fisik Belum Di Isi</h2>";			
		}else if($post['sarana_penunjang'] == ""){
			echo "<h2 style='color: red'>Nilai Sarana Penunjang Belum Di Isi</h2>";			
		}else if($post['produktifitas_tanam'] == ""){
			echo "<h2 style='color: red'>Nilai Produktifitas Tanam Belum Di Isi</h2>";			
		}else if($post['organisasi_personalia'] == ""){
			echo "<h2 style='color: red'>Nilai Organisasi Personalia Belum Di Isi</h2>";
		}else if($post['dokumentasi'] == ""){
			echo "<h2 style='color: red'>Dokumentasi Belum Di Isi</h2>";
		}else if($post['p3a'] == ""){
			echo "<h2 style='color: red'>Nilai P3A Belum Di Isi</h2>";
		// }else if($post['anggaran_operasional'] == ""){
		// 	echo "<h2 style='color: red'>Anggaran Operasional Belum Di Isi</h2>";
		// }else if($post['anggaran_perbaikan'] == ""){
		// 	echo "<h2 style='color: red'>Anggaran Perbaikan Belum Di Isi</h2>";
		// }else if($post['anggaran_pergantian'] == ""){
		// 	echo "<h2 style='color: red'>Anggaran Pergantian Belum Di Isi</h2>";
		}else{
			if (!empty($post['kategori_aknop'])) {
				$kategori = 1;
			}else{
				$kategori = 0;
			}
			$data = array('nama_di' 				=> $post['nama_irigasi'],
						  'prasarana_fisik'			=> $post['prasarana_fisik'],
						  'sarana_penunjang'		=> $post['sarana_penunjang'],
						  'produktifitas_tanam'		=> $post['produktifitas_tanam'],
						  'organisasi_personalia'	=> $post['organisasi_personalia'],
						  'dokumentasi'				=> $post['dokumentasi'],
						  'p3a'						=> $post['p3a'],
						  'tahun_anggaran'			=> $post['tahun_anggaran'],
						  'anggaran_operasional'	=> $post['anggaran_operasional'],
						  'anggaran_perbaikan'		=> $post['anggaran_perbaikan'],
						  'anggaran_pergantian'		=> $post['anggaran_pergantian'],
						  'kategori_aknop'			=> $kategori,
						  'txtInsertedBy'			=> $this->session->userdata('username'),
						  'dtmInsertedDate'			=> date('Y-m-d H:i:s')
						 );
			$simpan = $this->Aknop_m->simpan_aknop($data);
			if ($simpan) {
				$get_max_aknop = $this->Aknop_m->get_max_aknop()->row();
				for ($i=0; $i < count($post['pelaku']); $i++) { 
					$aktor[] = array('id_aknop_trAknop'	=> $get_max_aknop->id_aknop,
									 'nama_aktor'		=> $post['pelaku'][$i],
									 'anggaran_aktor'	=> $post['anggaran_pelaku'][$i],
									 'txtInsertedBy'	=> $this->session->userdata('username'),
									 'dtmInsertedDate'	=> date('Y-m-d H:i:s')
									);
				}
				$simpan_aktor = $this->Aknop_m->simpan_aktor($aktor);
				if ($simpan_aktor) {
					echo "Berhasil";
				}else{
					echo var_dump($this->db->error_log());
				}
			}else{
				echo var_dump($this->db->error_log());
			}

		}
		// exit(var_dump($post));
		// code...
	}


	public function update_aknop()
	{
		$post = $this->input->post();
		if ($post['nama_irigasi'] == "") {
			echo "<h2 style='color: red'>Nama Irigasi Belum Di Isi</h2>";
		}else if ($post['tahun_anggaran'] == "") {
			echo "<h2 style='color: red'>Tahun Anggaran Belum Di Isi</h2>";
		}else if($post['prasarana_fisik'] == ""){
			echo "<h2 style='color: red'>Nilai Prasaranan Fisik Belum Di Isi</h2>";			
		}else if($post['sarana_penunjang'] == ""){
			echo "<h2 style='color: red'>Nilai Sarana Penunjang Belum Di Isi</h2>";			
		}else if($post['produktifitas_tanam'] == ""){
			echo "<h2 style='color: red'>Nilai Produktifitas Tanam Belum Di Isi</h2>";			
		}else if($post['organisasi_personalia'] == ""){
			echo "<h2 style='color: red'>Nilai Organisasi Personalia Belum Di Isi</h2>";
		}else if($post['dokumentasi'] == ""){
			echo "<h2 style='color: red'>Dokumentasi Belum Di Isi</h2>";
		}else if($post['p3a'] == ""){
			echo "<h2 style='color: red'>Nilai P3A Belum Di Isi</h2>";
		}else if($post['anggaran_operasional'] == ""){
			echo "<h2 style='color: red'>Anggaran Operasional Belum Di Isi</h2>";
		}else{
			if (!empty($post['kategori_aknop'])) {
				$kategori = 1;
			}else{
				$kategori = 0;
			}
			$data = array('id_aknop'				=> $post['id_aknop'],
						  'nama_di' 				=> $post['nama_irigasi'],
						  'prasarana_fisik'			=> $post['prasarana_fisik'],
						  'sarana_penunjang'		=> $post['sarana_penunjang'],
						  'produktifitas_tanam'		=> $post['produktifitas_tanam'],
						  'organisasi_personalia'	=> $post['organisasi_personalia'],
						  'dokumentasi'				=> $post['dokumentasi'],
						  'p3a'						=> $post['p3a'],
						  'tahun_anggaran'			=> $post['tahun_anggaran'],
						  'anggaran_operasional'	=> $post['anggaran_operasional'],
						  'anggaran_perbaikan'		=> $post['anggaran_perbaikan'],
						  'anggaran_pergantian'		=> $post['anggaran_pergantian'],
						  'kategori_aknop'			=> $kategori,
						  'txtUpdatedBy'			=> $this->session->userdata('username'),
						  'dtmUpdatedDate'			=> date('Y-m-d H:i:s')
						 );
			$simpan = $this->Aknop_m->update_aknop($data);
			if ($simpan) {
				// $get_max_aknop = $this->Aknop_m->get_max_aknop()->row();
				for ($i=0; $i < count($post['pelaku']); $i++) { 
					$aktor[] = array('id_aknop_trAknop'	=> $post['id_aknop'],
									 'nama_aktor'		=> $post['pelaku'][$i],
									 'anggaran_aktor'	=> $post['anggaran_pelaku'][$i],
									 'txtUpdatedBy'		=> $this->session->userdata('username'),
									 'dtmUpdatedDate'	=> date('Y-m-d H:i:s')
									);
				}
				$simpan_aktor = $this->Aknop_m->simpan_aktor($aktor);
				if ($simpan_aktor) {
					echo "Berhasil";
				}else{
					echo var_dump($this->db->error_log());
				}
			}else{
				echo var_dump($this->db->error_log());
			}

		}
	}

	public function delete_aknop()
	{
		$post = $this->input->post();
		$simpan = $this->Aknop_m->delete_aknop($post);
		if ($simpan) {
			echo "Berhasil";
		}else{
			echo var_dump($this->db->error_log());
		}
	}


	public function get_aknop_ajax()
	{
		$post = $this->input->post();
		$data = $this->Aknop_m->get_aknop_detail($post['id_aknop']);
		echo json_encode($data->result());
		

	}
}
