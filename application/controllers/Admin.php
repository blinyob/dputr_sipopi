<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{
		parent:: __construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
        $this->sipelnas = $this->load->database('sipelnas',TRUE);
		$this->load->model('Login_m');
		$this->load->model('Kegiatan_M');
		$this->load->model('Aknop_m');
	}
	public function index()
	{
		$data['content'] = 'admin';
		$this->load->view('index',$data);
	}
	public function data_proyek($status = NULL)
	{
		if ($status != NULL) {
			$this->session->set_flashdata('success','Anda berhasil menambahkan data proyek');
		}else{
			$this->session->set_flashdata('');
		}
		$data['data_proyek'] = $this->Kegiatan_M->get_proyek();
		// $data['data_proyek'] = $this->Login_m->getProyek();
		$data['content'] = 'daftar_proyek';
		$this->load->view('index',$data);
	}
	public function data_lengkap_proyek($status = NULL)
	{
		// $data['data_proyek'] = $this->Kegiatan_M->get_proyek();
		// $data['data_proyek'] = $this->Login_m->getProyek();
		$data['kegiatan']=$this->Kegiatan_M->get_kegiatan();
		$data['content'] = 'daftar_lengkap_proyek';
		$this->load->view('index',$data);
	}
	public function data_proyek_selesai()
	{
		$data['data_proyek'] = $this->Login_m->getProyekSelesai();
		$data['content'] = 'daftar_proyek';
		$this->load->view('index',$data);
	}
	public function data_proyek_proses()
	{
		$data['data_proyek'] = $this->Login_m->getProyekProses();
		$data['content'] = 'daftar_proyek';
		$this->load->view('index',$data);
	}
	public function detail_proyek($id,$status = NULL)
	{
		// $data['data_proyek'] = $this->Login_m->getProyek();
		// $data['proyekById'] = $this->Login_m->getViewProyekById($id);
		if ($status != NULL) {
			$this->session->set_flashdata('success','Anda berhasil menambahkan progress proyek');
		}else{
			$this->session->set_flashdata('');
		}
		$get_max_laporan = $this->Kegiatan_M->get_max_laporan($id)->row();
		// exit(var_dump($get_max_laporan));
		$data['proyekById'] = $this->Kegiatan_M->get_proyek_detail($id,$get_max_laporan->laporan)->row_array();
		$data['proyekDetail'] = $this->Kegiatan_M->get_proyek_detail($id,$get_max_laporan->laporan);
		$data['list_file']= $this->Login_m->getAllfile($id);
		$data['id_monitoring'] = $id;
		// exit(var_dump($id));
		$data['content'] = 'detail_proyek_dropzone';
		$this->load->view('index',$data);
	}
	public function tambah_data_proyek()
	{
		$data['jalan'] = $this->Login_m->getJalan();
		$data['kegiatan']=$this->Kegiatan_M->get_kegiatan();
		$data['satkers']=$this->Login_m->getSatkers();
		$data['kontraktor']=$this->Login_m->getKontraktor();
		$data['content'] = 'tambah_proyek';
		$this->load->view('index',$data);
	}
	public function pengguna()
	{
		if($this->session->tipe!='administrator'){
			redirect(base_url('admin'));
		}
		$data['users'] = $this->Login_m->getPengguna();
		$data['content'] = 'pengguna';
		$this->load->view('index',$data);
	}
	public function tambah_pengguna()
	{
		$data['content'] = 'tambah_pengguna';
		$this->load->view('index',$data);
	}
	public function edit_pengguna($id)
	{
		$data['userById'] = $this->Login_m->getUserById($id); 
		$data['content'] = 'edit_pengguna';
		$this->load->view('index',$data);
	}

	public function kontraktor()
	{
		if($this->session->tipe!='administrator'){
			redirect(base_url('admin'));
		}
		$data['users'] = $this->Login_m->getKontraktor();
		$data['content'] = 'kontraktor';
		$this->load->view('index',$data);
	}
	public function tambah_kontraktor()
	{
		$data['content'] = 'tambah_kontraktor';
		$this->load->view('index',$data);
	}
	public function edit_kontraktor($id)
	{
		$data['kontraktorById'] = $this->Login_m->getKontraktorById($id); 
		$data['content'] = 'edit_kontraktor';
		$this->load->view('index',$data);
	}


	public function jalan()
	{
		if($this->session->tipe!='administrator'){
			redirect(base_url('admin'));
		}
		$data['jalan'] = $this->Login_m->getJalan();
		$data['content'] = 'jalan';
		$this->load->view('index',$data);
	}
	// public function tambah_kontraktor()
	// {
	// 	$data['content'] = 'tambah_kontraktor';
	// 	$this->load->view('index',$data);
	// }
	// public function edit_kontraktor($id)
	// {
	// 	$data['kontraktorById'] = $this->Login_m->getKontraktorById($id); 
	// 	$data['content'] = 'edit_kontraktor';
	// 	$this->load->view('index',$data);
	// }
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	public function edit_proyek($id)
	{
		// $data['satkers']=$this->Login_m->getSatkers();
		// $data['kontraktor']=$this->Login_m->getKontraktor();
		// $data['proyekById'] = $this->Login_m->getProyekById($id); 
		// var_dump(date('d/m/Y',strtotime($data['proyekById']['tanggal_proyek'])));die;
		$get_max_laporan = $this->Kegiatan_M->get_max_laporan($id)->row();
		$data['jalan'] = $this->Login_m->getJalan();
		$data['kegiatan']=$this->Kegiatan_M->get_kegiatan();
		$data['proyekById'] = $this->Kegiatan_M->get_proyek_detail($id,$get_max_laporan->laporan)->row_array();
		$data['content'] = 'edit_proyek';
		$this->load->view('index',$data);
	}


	public function aknop()
	{
		$data['content'] = 'daftar_aknop';
		// $data['aknop'] = $this->Aknop_m->get_aknop();
		$data['aknop'] = $this->Aknop_m->data_aknop();
		$data['irigasi'] = $this->Aknop_m->get_irigasi();
		$this->load->view('index',$data);
	}

	public function tambah_data_aknop()
	{
		$data['content'] = 'tambah_aknop';
		$this->load->view('index',$data);
	}

////////////////////////////////////////////////////////////

	public function irigasi()
	{
		$data['content'] = 'irigasi';
		// $data['aknop'] = $this->Aknop_m->get_aknop();irigasi
		$data['irigasi'] = $this->Aknop_m->get_irigasi();
		$this->load->view('index',$data);
	}
}