<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek extends CI_Controller {
	function __construct()
	{
		parent:: __construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
        $this->sipelnas = $this->load->database('sipelnas',TRUE);
		$this->load->model('Kegiatan_M');
		// $this->load->model('Login_m');
	}
	public function index()
	{
		$this->load->view('daftar_proyek');
	}

	public function get_kegiatan_detail()
	{
		$post = $this->input->post('kegiatan');
		$data = $this->Kegiatan_M->get_kegiatan_detail($post);
		echo json_encode($data->result());
		// var_dump($data->result());
	}

	public function get_proyek_detail()
	{
		$post = $this->input->post();
		$data = $this->Kegiatan_M->get_proyek_detail_file($post);
		echo json_encode($data->result());
		// var_dump($data->result());
	}

	public function simpan_proyek()
	{
		$post = $this->input->post();
		if ($post['kode_program'] == "") {
			echo "<h2 style='color: red'>Kegiatan Belum Di Pilih</h2>";
		}else if ($post['ruas_jalan'] == "") {
			echo "<h2 style='color: red'>Ruas Jalan Belum Di Isi</h2>";
		}else if($post['lokasi'] == ""){
			echo "<h2 style='color: red'>Lokasi Belum Di Isi</h2>";			
		}else if($post['sta'] == ""){
			echo "<h2 style='color: red'>STA Belum Di Isi</h2>";			
		}else if($post['panjang_ruas_jalan'] == ""){
			echo "<h2 style='color: red'>Panjang Ruas Jalan Belum Di Isi</h2>";			
		}else if($post['lebar_ruas_jalan'] == ""){
			echo "<h2 style='color: red'>Lebar Ruas Jalan Belum Di Isi</h2>";			
		}else if($post['jenis_kontruksi'] == ""){
			echo "<h2 style='color: red'>Jenis Kontruksi Belum Di Isi</h2>";			
		}else if($post['nilai_kontrak'] == ""){
			echo "<h2 style='color: red'>Nilai Kontrak Belum Di Isi</h2>";			
		}else if($post['nomor_spk'] == ""){
			echo "<h2 style='color: red'>Nomor SPK Belum Di Isi</h2>";			
		}else if($post['tgl_spk'] == ""){
			echo "<h2 style='color: red'>Tanggal SPK Belum Di Isi</h2>";			
		}else if($post['waktu_pelaksanaan'] == ""){
			echo "<h2 style='color: red'>Waktu Pelaksanaan Belum Di Isi</h2>";			
		}else if($post['spmk_awal'] == ""){
			echo "<h2 style='color: red'>SPMK Awal Belum Di Isi</h2>";			
		}else if($post['spmk_akhir'] == ""){
			echo "<h2 style='color: red'>SPMK Akhir Belum Di Isi</h2>";			
		}else if($post['penyedia_jasa'] == ""){
			echo "<h2 style='color: red'>Penyedia Jasa Belum Di Isi</h2>";			
		}else if($post['pengawas'] == ""){
			echo "<h2 style='color: red'>Pengawas Belum Di Isi</h2>";			
		}else if($post['rencana'] == ""){
			echo "<h2 style='color: red'>Perencanaan Belum Di Isi</h2>";			
		}else{
			$data = array('kode_program' 					=> $post['kode_program'],
						  'kode_kegiatan'					=> $post['kode_kegiatan'],
						  'kode_sub_kegiatan'				=> $post['kode_sub_kegiatan'],
						  'id_sub_kegiatan_dtSubKegiatan'	=> $post['id_sub_kegiatan'],
						  'pagu_anggaran'					=> $post['anggaran'],
						  'ruas_jalan'						=> $post['ruas_jalan'],
						  'STA'								=> $post['sta'],
						  'lokasi'							=> $post['lokasi'],
						  'nilai_kontrak'					=> $post['nilai_kontrak'],
						  'jenis_kontruksi'					=> $post['jenis_kontruksi'],
						  'panjang_ruas_jalan'				=> $post['panjang_ruas_jalan'],
						  'lebar_ruas_jalan'				=> $post['lebar_ruas_jalan'],
						  'nomor_spk'						=> $post['nomor_spk'],
						  'tanggal_spk'						=> $post['tgl_spk'],
						  'waktu_pelaksanaan'				=> $post['waktu_pelaksanaan'],
						  'tanggal_mulai'					=> $post['spmk_awal'],
						  'tanggal_akhir'					=> $post['spmk_akhir'],
						  'penyedia_jasa'					=> $post['penyedia_jasa'],
						  'pengawas'						=> $post['pengawas'],
						  'rencana'							=> $post['rencana'],
						  'keterangan'						=> $post['keterangan'],
						 );
			$simpan = $this->Kegiatan_M->simpan_proyek($data);
			// $id_proyek = $this->Kegiatan_M->get_id_proyek();
			// $detail = array('id_monitoring_trMonitoring' => $id_proyek->id_monitoring, 
			// 				'status'					 => '0',
			// 				'keterangan'				 => 'inisialisasi Proyek'
			// 			   );
			// $simpan_detail = $this->Kegiatan_M->simpan_detail_proyek($detail);
			if ($simpan) {
				echo "Berhasil";
			}else{
				echo var_dump($this->db->error_log());
			}
		}
		// var_dump($post);
	}

	public function edit_proyek()
	{
		$post = $this->input->post();
		if ($post['kode_program'] == "") {
			echo "<h2 style='color: red'>Kegiatan Belum Di Pilih</h2>";
		}else if ($post['ruas_jalan'] == "") {
			echo "<h2 style='color: red'>Ruas Jalan Belum Di Isi</h2>";
		}else if($post['lokasi'] == ""){
			echo "<h2 style='color: red'>Lokasi Belum Di Isi</h2>";			
		}else if($post['sta'] == ""){
			echo "<h2 style='color: red'>STA Belum Di Isi</h2>";			
		}else if($post['panjang_ruas_jalan'] == ""){
			echo "<h2 style='color: red'>Panjang Ruas Jalan Belum Di Isi</h2>";			
		}else if($post['lebar_ruas_jalan'] == ""){
			echo "<h2 style='color: red'>Lebar Ruas Jalan Belum Di Isi</h2>";			
		}else if($post['jenis_kontruksi'] == ""){
			echo "<h2 style='color: red'>Jenis Kontruksi Belum Di Isi</h2>";			
		}else if($post['nilai_kontrak'] == ""){
			echo "<h2 style='color: red'>Nilai Kontrak Belum Di Isi</h2>";			
		}else if($post['nomor_spk'] == ""){
			echo "<h2 style='color: red'>Nomor SPK Belum Di Isi</h2>";			
		}else if($post['tgl_spk'] == ""){
			echo "<h2 style='color: red'>Tanggal SPK Belum Di Isi</h2>";			
		}else if($post['waktu_pelaksanaan'] == ""){
			echo "<h2 style='color: red'>Waktu Pelaksanaan Belum Di Isi</h2>";			
		}else if($post['spmk_awal'] == ""){
			echo "<h2 style='color: red'>SPMK Awal Belum Di Isi</h2>";			
		}else if($post['spmk_akhir'] == ""){
			echo "<h2 style='color: red'>SPMK Akhir Belum Di Isi</h2>";			
		}else if($post['penyedia_jasa'] == ""){
			echo "<h2 style='color: red'>Penyedia Jasa Belum Di Isi</h2>";			
		}else if($post['pengawas'] == ""){
			echo "<h2 style='color: red'>Pengawas Belum Di Isi</h2>";			
		}else if($post['rencana'] == ""){
			echo "<h2 style='color: red'>Perencanaan Belum Di Isi</h2>";			
		}else{
			$data = array('id_monitoring'					=> $post['id_monitoring'],
						  'kode_program' 					=> $post['kode_program'],
						  'kode_kegiatan'					=> $post['kode_kegiatan'],
						  'kode_sub_kegiatan'				=> $post['kode_sub_kegiatan'],
						  'id_sub_kegiatan_dtSubKegiatan'	=> $post['id_sub_kegiatan'],
						  'pagu_anggaran'					=> $post['anggaran'],
						  'ruas_jalan'						=> $post['ruas_jalan'],
						  'STA'								=> $post['sta'],
						  'lokasi'							=> $post['lokasi'],
						  'nilai_kontrak'					=> $post['nilai_kontrak'],
						  'jenis_kontruksi'					=> $post['jenis_kontruksi'],
						  'panjang_ruas_jalan'				=> $post['panjang_ruas_jalan'],
						  'lebar_ruas_jalan'				=> $post['lebar_ruas_jalan'],
						  'nomor_spk'						=> $post['nomor_spk'],
						  'tanggal_spk'						=> $post['tgl_spk'],
						  'waktu_pelaksanaan'				=> $post['waktu_pelaksanaan'],
						  'tanggal_mulai'					=> $post['spmk_awal'],
						  'tanggal_akhir'					=> $post['spmk_akhir'],
						  'penyedia_jasa'					=> $post['penyedia_jasa'],
						  'pengawas'						=> $post['pengawas'],
						  'rencana'							=> $post['rencana'],
						  'keterangan'						=> $post['keterangan'],
						 );
			$simpan = $this->Kegiatan_M->edit_proyek($data);
			// $id_proyek = $this->Kegiatan_M->get_id_proyek();
			// $detail = array('id_monitoring_trMonitoring' => $id_proyek->id_monitoring, 
			// 				'status'					 => '0',
			// 				'keterangan'				 => 'inisialisasi Proyek'
			// 			   );
			// $simpan_detail = $this->Kegiatan_M->simpan_detail_proyek($detail);
			if ($simpan) {
				echo "Berhasil";
			}else{
				echo var_dump($this->db->error_log());
			}
		}
		// var_dump($post);
	}

	function delete_proyek()
	{
		$post = $this->input->post();
			$simpan_detail = $this->Kegiatan_M->delete_proyek($post);
			if ($simpan_detail) {
				echo "Berhasil";
			}else{
				echo var_dump($this->db->error_log());
			}

	}



	function simpan_detail()
	{
		$uploadDir = 'assets/foto_proyek';
		$post = $this->input->post();
		$get_max_laporan = $this->Kegiatan_M->get_max_laporan($post['id_monitoring'])->row();
		$data[] = "";

		if (empty($post['status'])) {
			$status = 'on progess';
		}else{
			$status = $post['status'];
		}

		if (!empty($_FILES)) {
			if ($post['PIC'] == "") {
				echo "<h2 style='color: red'>Pemeriksa tidak boleh kosong</h2>";
				exit();
			}else if ($post['persentase'] == "") {
				echo "<h2 style='color: red'>Persentase tidak boleh kosong</h2>";
				exit();
			}else if ($post['rincian'] == "") {
				echo "<h2 style='color: red'>Perincian tidak boleh kosong</h2>";
				exit();
			}else{
				for ($i=0; $i < count($_FILES['file']['name']); $i++) { 
					$namafile = $_FILES['file']['name'][$i];
					$tmpFile = $_FILES['file']['tmp_name'][$i];
			 		// var_dump($namafile,$tmpfile);

					// $tmpFile = $_FILES['file']['tmp_name'];
					$filename = $uploadDir.'/'.time().'-'. $namafile;
					if (move_uploaded_file($tmpFile,$filename)) {
					$data[$i] = array('id_monitoring_trMonitoring' => $post['id_monitoring'], 
									  'pemeriksa'				   => $post['PIC'],
									  'persentase_detail'		   => $post['persentase'],
									  'status'					   => $status,
									  'file'					   => $filename,
									  'tgl_laporan'				   => date('Y-m-d'),
									  'laporan_ke'				   => $get_max_laporan->laporan+1,
									  'keterangan_detail'		   => $post['rincian'],
									 );
					}else{
						echo "gagal upload";
					}
				}
				$simpan_detail = $this->Kegiatan_M->simpan_detail_proyek($data);
				if ($simpan_detail) {
					echo "Berhasil";
				}else{
					echo var_dump($this->db->error_log());
				}

			}
		}else{
			echo "<h2 style='color: red'>Harap upload bukti foto</h2>";
			exit();
		}
	}

	function edit_detail()
	{
		$post = $this->input->post();
		$get_max_laporan = $this->Kegiatan_M->get_max_laporan($post['id_monitoring'])->row();
		if (empty($post['status'])) {
			$status = 'on progess';
		}else{
			$status = $post['status'];
		}

		if ($post['PIC'] == "") {
			echo "<h2 style='color: red'>Pemeriksa tidak boleh kosong</h2>";
			exit();
		}else if ($post['persentase'] == "") {
			echo "<h2 style='color: red'>Persentase tidak boleh kosong</h2>";
			exit();
		}else if ($post['rincian'] == "") {
			echo "<h2 style='color: red'>Perincian tidak boleh kosong</h2>";
			exit();
		}else{
			$data = array('id_monitoring_trMonitoring' => $post['id_monitoring'], 
						  'pemeriksa'				   => $post['PIC'],
						  'persentase_detail'		   => $post['persentase'],
						  'laporan_ke'				   => $post['laporan'],
						  'status'					   => $status,
						  'tgl_laporan'				   => date('Y-m-d'),
						  'keterangan_detail'		   => $post['rincian'],
						 );
		}
		$simpan_detail = $this->Kegiatan_M->edit_detail_proyek($data);
		if ($simpan_detail) {
			if ($get_max_laporan->laporan == $post['laporan']) {
				$update_persentase = $this->Kegiatan_M->update_persentase($data);
				if ($update_persentase) {
					echo "Berhasil";
				}else{
					echo "Berhasil";
					// echo var_dump($this->db->error_log());
				}
			}
		}else{
			echo var_dump($this->db->error_log());
		}

	}


	function delete_detail()
	{
		$post = $this->input->post();
		$cek_laporan = $this->Kegiatan_M->get_max_laporan($post['id_monitoring'])->row();
		if ($post['laporan'] < $cek_laporan->laporan) {
			echo "<h2 style='color: red'>Anda hanya bisa menghapus data progress terakhir</h2>";
			exit();
		}else{
			$simpan_detail = $this->Kegiatan_M->delete_detail_proyek($post);
			if ($simpan_detail) {
				$get_max_laporan = $this->Kegiatan_M->get_max_laporan($post['id_monitoring'])->row_array();
				$update_persentase = $this->Kegiatan_M->update_persentase($get_max_laporan);
				if ($update_persentase) {
					echo "Berhasil";
				}else{
					echo var_dump($this->db->error_log());
				}
			}else{
				echo var_dump($this->db->error_log());
			}
		}

	}

	public function report_pdf()
	{
		$post = $this->input->post();
		$data['kegiatan'] 	= $this->Kegiatan_M->get_kegiatan();
		$data['bulan']		= $post['bulan'];
		$data['nama_bulan']	= $bulan=array("empty","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
		$this->load->view('report',$data);
	}


	public function report_with_plugin()
	{
		// session_start();
		// ini_set('max_execution_time', 0); 
		// ini_set('memory_limit','2048M'); 
		// require_once './assets/conf/functions.php';
		// require_once './assets/conf/conn.php';
		// require_once './assets/conf/Classes/PHPExcel.php';
   		include APPPATH.'third_party/PHPExcel.php';
		
		// Create new PHPExcel object
		// $nik_koordinator = $_POST['nik_koordinator'];
		$post = $this->input->post();
		$kegiata = $this->Kegiatan_M->get_kegiatan();
		$object = new PHPExcel();
		$sharedStyle1 = new PHPExcel_Style();  
		 
		$counter=3;
		$no=0;
		$ex = $object->setActiveSheetIndex(0); 
		foreach ($kegiatan as $row) {
		          $ex->setCellValue("A".$counter,@$data[0]['no_registrasi']);
		          $ex->setCellValue("B".$counter,@$data[0]['bulan_reg']);
		          $ex->setCellValue("C".$counter,@$data[0]['nama_cg']);
		          $ex->setCellValue("D".$counter,@$data[1]['nama_cg']);
		          $ex->setCellValue("E".$counter,@$data[2]['nama_cg']);
		          $ex->setCellValue("F".$counter,@$data[3]['nama_cg']);
		          $ex->setCellValue("G".$counter,@$data[4]['nama_cg']);
		          $ex->setCellValue("H".$counter,@$data[0]['tgl_usul']);
		          $ex->setCellValue("I".$counter,@$data[0]['nama_karyawan']);
		          $ex->setCellValue("J".$counter,@$data[1]['nama_karyawan']);
		          $ex->setCellValue("K".$counter,@$data[2]['nama_karyawan']);
		          $ex->setCellValue("L".$counter,@$data[3]['nama_karyawan']);
		          $ex->setCellValue("M".$counter,@$data[4]['nama_karyawan']);
		          $ex->setCellValue("N".$counter,@$data[0]['latar_belakang']);
		          $ex->setCellValue("O".$counter,@$data[0]['perbaikan']);
		          $ex->setCellValue("P".$counter,getDecimal(@$data[0]['bln_imp']));
		          $ex->setCellValue("Q".$counter,@$data[0]['tema']);
		          $ex->setCellValue("R".$counter,@$data[0]['no_opl']);
		          $ex->setCellValue("S".$counter,$d1['poin']);
		          $counter=$counter+1;
		          //var_dump($_POST['period_2']);die();
		}

		$object->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$object->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$object->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('P')->setWidth(10);
		$object->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$object->getActiveSheet()->getColumnDimension('S')->setWidth(10);

		$object->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);



		$object->getActiveSheet()->mergeCells('C1:G1');
		$object->getActiveSheet()->mergeCells('I1:M1');
		$object->getActiveSheet()->mergeCells('A1:A2');
		$object->getActiveSheet()->mergeCells('B1:B2');
		$object->getActiveSheet()->mergeCells('H1:H2');
		$object->getActiveSheet()->mergeCells('N1:N2');
		$object->getActiveSheet()->mergeCells('O1:O2');
		$object->getActiveSheet()->mergeCells('P1:P2');
		$object->getActiveSheet()->mergeCells('Q1:Q2');
		$object->getActiveSheet()->mergeCells('R1:R2');
		$object->getActiveSheet()->mergeCells('S1:S2');
		// $object->getActiveSheet()->mergeCells('O1:O2');

		$object->setActiveSheetIndex(0)
		            ->setCellValue('C1', 'Circle Group')
		            ->setCellValue('I1', 'Nama Karyawan')
		            ->setCellValue('A1', 'No.Registrasi')
		            ->setCellValue('B1', 'Bulan REG')
		            ->setCellValue('C2', 'PIC 1')
		            ->setCellValue('D2', 'PIC 2')
		            ->setCellValue('E2', 'PIC 3')
		            ->setCellValue('F2', 'PIC 4')
		            ->setCellValue('G2', 'PIC 5')
		            ->setCellValue('H1', 'Tanggal Usulan')
		            ->setCellValue('I2', 'PIC 1')
		            ->setCellValue('J2', 'PIC 2')
		            ->setCellValue('K2', 'PIC 3')
		            ->setCellValue('L2', 'PIC 4')
		            ->setCellValue('M2', 'PIC 5')
		            ->setCellValue('N1', 'Latar Belakang')
		            ->setCellValue('O1', 'Usulan Perbaikan')
		            ->setCellValue('P1', 'Bulan IMP')
		            ->setCellValue('Q1', 'Tema')
		            ->setCellValue('R1', 'No.OPL')
		      		->setCellValue('S1', 'POIN SS');
		 
		$sharedStyle1->applyFromArray(
		 array('borders' => array(
		 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
		 'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
		 ),
		 ));

		// Set page orientation and size
		$object->getActiveSheet()->setSharedStyle($sharedStyle1, "A1:N$counter");
		$object->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$object->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

		// Rename second worksheet

		$object->getActiveSheet()->setTitle('Laporan');
		 
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$object->setActiveSheetIndex(0);
		$namafile=gmdate("Y-m-d H:i:s", time()+60*60*7);
		 
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$namafile.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save('php://output');
		exit;
	}
}