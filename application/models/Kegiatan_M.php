<?php

class Kegiatan_M extends CI_Model {

	public function get_kegiatan()
	{
		$this->sipelnas->select('
								 dt_sub_kegiatan.id_sub_kegiatan,
								 kode_program,
								 kode_kegiatan,
								 kode_sub_kegiatan,
								 nama_sub_kegiatan,
								 pagu
								');
		$this->sipelnas->from('dt_sub_kegiatan');
		$this->sipelnas->join('dt_kegiatan','dt_sub_kegiatan.id_kegiatan=dt_kegiatan.id_kegiatan','LEFT');
		$this->sipelnas->join('dt_program','dt_kegiatan.id_program=dt_program.id_program','LEFT');
		$this->sipelnas->group_by('kode_sub_kegiatan','ASC');
		$this->sipelnas->order_by('kode_kegiatan','ASC');
		$this->sipelnas->order_by('kode_program','ASC');
		return $this->sipelnas->get();
	}

	public function get_kegiatan_detail($id_kegiatan)
	{
		$this->sipelnas->select('
								 dt_sub_kegiatan.id_sub_kegiatan,
								 kode_program,
								 kode_kegiatan,
								 kode_sub_kegiatan,
								 nama_sub_kegiatan,
								 pagu,
								');
		$this->sipelnas->from('dt_sub_kegiatan');
		$this->sipelnas->join('dt_kegiatan','dt_sub_kegiatan.id_kegiatan=dt_kegiatan.id_kegiatan');
		$this->sipelnas->join('dt_program','dt_kegiatan.id_program=dt_program.id_program');
		$this->sipelnas->where('id_sub_kegiatan',$id_kegiatan);
		$this->sipelnas->group_by('kode_sub_kegiatan','ASC');
		$this->sipelnas->order_by('kode_program','ASC');
		return $this->sipelnas->get();
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function get_id_proyek()
	{
		$this->db->select('MAX(id_monitoring) AS id_monitoring');
		$this->db->from('tr_monitoring');
		return $this->db->get()->row();
	}


	public function get_proyek()
	{
		$this->db->select('*');
		$this->db->from('tr_monitoring');
		return $this->db->get();
	}

	public function get_proyek_detail($id_monitoring,$laporan)
	{

		$this->db->select('*');
		$this->db->from('tr_monitoring');
		$this->db->join('tr_monitoring_detail','tr_monitoring.id_monitoring=tr_monitoring_detail.id_monitoring_trMonitoring','LEFT');
		$this->db->where('id_monitoring',$id_monitoring);
		if ($laporan != NULL) {
			$this->db->where('bitActive',1);
		}
		// $this->db->order_by()
		$this->db->group_by('laporan_ke','ASC');
		return $this->db->get();
	}

	function simpan_proyek($data)
	{
		$this->db->insert('tr_monitoring',$data);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function edit_proyek($data)
	{
		// exit(var_dump($data));
		$this->db->where('id_monitoring',$data['id_monitoring']);
		$this->db->update('tr_monitoring',$data);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function delete_proyek($data)
	{
		// exit(var_dump($data));
		$this->db->where('id_monitoring',$data['id_monitoring']);
		$this->db->delete('tr_monitoring');
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function update_persentase($data)
	{
		$this->db->set('persentase',$data['persentase_detail']);
		$this->db->where('id_monitoring',$data['id_monitoring_trMonitoring']);
		$this->db->update('tr_monitoring');
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function get_kegiatan_proyek($id_sub_kegiatan)
	{
		$this->db->select('*');
		$this->db->from('tr_monitoring');
		$this->db->where('id_sub_kegiatan_dtSubKegiatan',$id_sub_kegiatan);
		return $this->db->get();
	}


	public function get_max_laporan($id_monitoring)
	{
		$this->db->select('MAX(laporan_ke) AS laporan,
			               id_monitoring_trMonitoring,
			               persentase_detail
			             ');
		$this->db->from('tr_monitoring_detail');
		$this->db->where('id_monitoring_trMonitoring',$id_monitoring);
		$this->db->where('bitActive',1);
		return $this->db->get();
	}

	public function get_proyek_detail_file($post)
	{
		$this->db->select('id_monitoring_trMonitoring,
						   pemeriksa,
						   persentase_detail,
						   keterangan_detail,
						   file AS lampiran,
						   laporan_ke');
		$this->db->from('tr_monitoring');
		$this->db->join('tr_monitoring_detail','tr_monitoring.id_monitoring=tr_monitoring_detail.id_monitoring_trMonitoring','LEFT');
		$this->db->where('id_monitoring',$post['id_monitoring']);
		$this->db->where('laporan_ke',$post['laporan']);
		$this->db->where('bitActive',1);
		// $this->db->order_by()
		// $this->db->group_by('laporan_ke','ASC');
		return $this->db->get();
	}



	function simpan_detail_proyek($detail)
	{
		$this->db->insert_batch('tr_monitoring_detail',$detail);
		if ($this->db->affected_rows() >= 1) {
			$this->db->set('persentase',$detail[0]['persentase_detail']);
			$this->db->where('id_monitoring',$detail[0]['id_monitoring_trMonitoring']);
			$this->db->update('tr_monitoring');
			if ($this->db->affected_rows() >= 1) {
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	function edit_detail_proyek($detail)
	{	
		$this->db->where('id_monitoring_trMonitoring',$detail['id_monitoring_trMonitoring']);
		$this->db->where('laporan_ke',$detail['laporan_ke']);
		$this->db->where('bitActive',1);
		$this->db->update('tr_monitoring_detail',$detail);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function delete_detail_proyek($detail)
	{	
		$this->db->set('bitActive',0);
		$this->db->where('id_monitoring_trMonitoring',$detail['id_monitoring']);
		$this->db->where('laporan_ke',$detail['laporan']);
		$this->db->update('tr_monitoring_detail');
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
