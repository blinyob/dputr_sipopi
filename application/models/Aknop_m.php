<?php

class Aknop_m extends CI_Model {

	public function get_aknop($id_aknop = NULL)
	{
		$this->db->select('*');
		$this->db->from('tr_aknop');
		if ($id_aknop != NULL) {
			$this->db->where('id_aknop',$id_aknop);
		}
		return $this->db->get();
	}

	public function data_aknop($id_aknop = null)
	{
		$this->db->select('SUM(anggaran_aktor) AS anggaran_aktor,
						   tr_aktor_aknop.bitActive,
						   tr_aknop.*
			        	  ');
		$this->db->from('tr_aknop');
		$this->db->join('tr_aktor_aknop','tr_aknop.id_aknop=tr_aktor_aknop.id_aknop_trAknop');
		$this->db->where('tr_aknop.bitActive',1);
		$this->db->where('tr_aktor_aknop.bitActive',1);
		$this->db->group_by('id_aknop','DESC');
		return $this->db->get();
	}

	public function get_aknop_detail($id_aknop)
	{
		$this->db->select('*');
		$this->db->from('tr_aknop');
		$this->db->join('tr_aktor_aknop','tr_aknop.id_aknop=tr_aktor_aknop.id_aknop_trAknop');
		$this->db->where('id_aknop',$id_aknop);
		$this->db->where('tr_aktor_aknop.bitActive',1);
		// $this->db->group_by('id_aktor_aknop','ASC');
		return $this->db->get();
	}

	public function get_max_aknop()
	{
		// $this->db->select('MAX(laporan_ke) AS laporan,
		// 	               id_monitoring_trMonitoring,
		// 	               persentase_detail
		// 	             ');
		$this->db->select('*');
		$this->db->from('tr_aknop');
		$this->db->where('bitActive',1);
		$this->db->order_by('id_aknop','DESC');
		return $this->db->get();
	}

	public function simpan_aknop($data)
	{
		$this->db->insert('tr_aknop',$data);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}


	public function update_aknop($data)
	{
		$this->db->where('id_aknop',$data['id_aknop']);
		$this->db->update('tr_aknop',$data);
		if ($this->db->affected_rows() >= 1) {
			$this->db->set('bitActive',0);
			$this->db->where('id_aknop_trAknop',$data['id_aknop']);
			$this->db->update('tr_aktor_aknop');
			if ($this->db->affected_rows() >= 1) {
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}


	public function delete_aknop($data)
	{
		$this->db->set('bitActive',0);
		$this->db->where('id_aknop',$data['id_aknop']);
		$this->db->update('tr_aknop');
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function simpan_aktor($aktor)
	{
		$this->db->insert_batch('tr_aktor_aknop',$aktor);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function get_irigasi($id_irigasi = NULL)
	{
		$this->db->select('*');
		$this->db->from('irigasi');
		if ($id_irigasi != NULL) {
			$this->db->where('id_irigasi',$id_irigasi);
		}
		$this->db->where('bitActive',1);
		return $this->db->get();
	}

	public function simpan_irigasi($data)
	{
		$this->db->insert('irigasi',$data);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function edit_irigasi($data)
	{
		$this->db->where('id_irigasi',$data['id_irigasi']);
		$this->db->update('irigasi',$data);
		if ($this->db->affected_rows() >= 1) {
			return TRUE;
		}else{
			return FALSE;
		}
	}



}