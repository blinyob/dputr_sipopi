-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 06, 2023 at 02:42 AM
-- Server version: 8.0.30
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u166913405_dputr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_aknop`
--

CREATE TABLE `tr_aknop` (
  `id_aknop` int NOT NULL,
  `nama_di` varchar(255) NOT NULL,
  `prasarana_fisik` float NOT NULL,
  `sarana_penunjang` float NOT NULL,
  `produktifitas_tanam` float NOT NULL,
  `organisasi_personalia` float NOT NULL,
  `dokumentasi` float NOT NULL,
  `p3a` float NOT NULL,
  `tahun_anggaran` year NOT NULL,
  `anggaran_operasional` bigint NOT NULL,
  `anggaran_perbaikan` bigint NOT NULL,
  `anggaran_pergantian` bigint NOT NULL,
  `kategori_aknop` int NOT NULL COMMENT '1=operasional\r\n0=perbaikan',
  `txtInsertedBy` varchar(100) NOT NULL,
  `dtmInsertedDate` datetime NOT NULL,
  `txtUpdatedBy` varchar(100) NOT NULL,
  `dtmUpdatedDate` datetime NOT NULL,
  `bitActive` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_aknop`
--

INSERT INTO `tr_aknop` (`id_aknop`, `nama_di`, `prasarana_fisik`, `sarana_penunjang`, `produktifitas_tanam`, `organisasi_personalia`, `dokumentasi`, `p3a`, `tahun_anggaran`, `anggaran_operasional`, `anggaran_perbaikan`, `anggaran_pergantian`, `kategori_aknop`, `txtInsertedBy`, `dtmInsertedDate`, `txtUpdatedBy`, `dtmUpdatedDate`, `bitActive`) VALUES
(1, 'Cikemangi Wetan 2', 38, 76, 45, 35, 27, 21, 2023, 53000000, 300000000, 52000000, 0, 'admin', '2023-10-06 02:20:02', '', '0000-00-00 00:00:00', b'0'),
(2, 'Cibolotok', 66, 78, 23, 18, 99, 72, 2024, 45000000, 40000000, 35000000, 0, 'admin', '2023-10-06 02:30:05', '', '0000-00-00 00:00:00', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tr_aktor_aknop`
--

CREATE TABLE `tr_aktor_aknop` (
  `id_aktor_aknop` int NOT NULL,
  `id_aknop_trAknop` int NOT NULL,
  `nama_aktor` varchar(100) NOT NULL,
  `anggaran_aktor` bigint NOT NULL,
  `bitActive` bit(1) NOT NULL DEFAULT b'1',
  `txtInsertedBy` varchar(100) NOT NULL,
  `dtmInsertedDate` datetime NOT NULL,
  `txtUpdatedBy` varchar(100) NOT NULL,
  `dtmUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_aktor_aknop`
--

INSERT INTO `tr_aktor_aknop` (`id_aktor_aknop`, `id_aknop_trAknop`, `nama_aktor`, `anggaran_aktor`, `bitActive`, `txtInsertedBy`, `dtmInsertedDate`, `txtUpdatedBy`, `dtmUpdatedDate`) VALUES
(1, 1, 'Staff', 1200000, b'0', 'admin', '2023-10-04 08:52:00', '', '0000-00-00 00:00:00'),
(2, 1, 'Tukang Arit', 5000000, b'0', 'admin', '2023-10-04 08:52:00', '', '0000-00-00 00:00:00'),
(3, 1, 'Tukang Kebon ', 10500000, b'0', 'admin', '2023-10-04 08:52:00', '', '0000-00-00 00:00:00'),
(4, 1, 'Staff', 1200000, b'0', 'admin', '2023-10-06 01:38:11', '', '0000-00-00 00:00:00'),
(5, 1, 'Tukang Arit', 5000000, b'0', 'admin', '2023-10-06 01:38:11', '', '0000-00-00 00:00:00'),
(6, 1, 'Tukang Kebon ', 10500000, b'0', 'admin', '2023-10-06 01:38:11', '', '0000-00-00 00:00:00'),
(7, 1, 'Staff', 1200000, b'1', 'admin', '2023-10-06 02:20:02', '', '0000-00-00 00:00:00'),
(8, 1, 'Tukang Arit', 5000000, b'1', 'admin', '2023-10-06 02:20:02', '', '0000-00-00 00:00:00'),
(9, 1, 'Tukang Kebon ', 10500000, b'1', 'admin', '2023-10-06 02:20:02', '', '0000-00-00 00:00:00'),
(10, 2, 'Staff', 10000000, b'0', 'admin', '2023-10-06 02:29:48', '', '0000-00-00 00:00:00'),
(11, 2, 'Tukang ngaduk', 5000000, b'0', 'admin', '2023-10-06 02:29:48', '', '0000-00-00 00:00:00'),
(12, 2, 'Pengawas', 3000000, b'0', 'admin', '2023-10-06 02:29:48', '', '0000-00-00 00:00:00'),
(13, 2, 'Sopir', 3000000, b'0', 'admin', '2023-10-06 02:29:48', '', '0000-00-00 00:00:00'),
(14, 2, 'Staff', 10000000, b'1', 'admin', '2023-10-06 02:30:05', '', '0000-00-00 00:00:00'),
(15, 2, 'Tukang ngaduk', 5000000, b'1', 'admin', '2023-10-06 02:30:05', '', '0000-00-00 00:00:00'),
(16, 2, 'Pengawas', 3000000, b'1', 'admin', '2023-10-06 02:30:05', '', '0000-00-00 00:00:00'),
(17, 2, 'Sopir', 3000000, b'1', 'admin', '2023-10-06 02:30:05', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_aknop`
--
ALTER TABLE `tr_aknop`
  ADD PRIMARY KEY (`id_aknop`);

--
-- Indexes for table `tr_aktor_aknop`
--
ALTER TABLE `tr_aktor_aknop`
  ADD PRIMARY KEY (`id_aktor_aknop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_aknop`
--
ALTER TABLE `tr_aknop`
  MODIFY `id_aknop` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_aktor_aknop`
--
ALTER TABLE `tr_aktor_aknop`
  MODIFY `id_aktor_aknop` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
