-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 03, 2023 at 09:32 AM
-- Server version: 8.0.30
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u166913405_dputr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_aknop`
--

CREATE TABLE `tr_aknop` (
  `id_aknop` int NOT NULL,
  `nama_di` varchar(255)NOT NULL,
  `prasarana_fisik` float NOT NULL,
  `sarana_penunjang` float NOT NULL,
  `produktifitas_tanam` float NOT NULL,
  `organisasi_personalia` float NOT NULL,
  `dokumentasi` float NOT NULL,
  `p3a` float NOT NULL,
  `tahun_anggaran` year NOT NULL,
  `anggaran_operasional` bigint NOT NULL,
  `anggaran_perbaikan` bigint NOT NULL,
  `anggaran_pergantian` bigint NOT NULL,
  `txtInsertedBy` varchar(100) NOT NULL,
  `dtmInsertedDate` datetime NOT NULL,
  `txtUpdatedBy` varchar(100) NOT NULL,
  `dtmUpdatedDate` datetime NOT NULL,
  `bitActive` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_aknop`
--

INSERT INTO `tr_aknop` (`id_aknop`, `nama_di`, `prasarana_fisik`, `sarana_penunjang`, `produktifitas_tanam`, `organisasi_personalia`, `dokumentasi`, `p3a`, `tahun_anggaran`, `anggaran_operasional`, `anggaran_perbaikan`, `anggaran_pergantian`, `txtInsertedBy`, `dtmInsertedDate`, `txtUpdatedBy`, `dtmUpdatedDate`, `bitActive`) VALUES
(1, 'Cikunyuk', 43, 65, 66, 43, 26, 23, 2023, 53000000, 0, 0, 'admin', '2023-10-03 09:12:55', '', '0000-00-00 00:00:00', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tr_aktor_aknop`
--

CREATE TABLE `tr_aktor_aknop` (
  `id_aktor_aknop` int NOT NULL,
  `id_aknop_trAknop` int NOT NULL,
  `nama_aktor` varchar(100) NOT NULL,
  `anggaran_aktor` bigint NOT NULL,
  `txtInsertedBy` varchar(100) NOT NULL,
  `dtmInsertedDate` datetime NOT NULL,
  `txtUpdatedBy` varchar(100) NOT NULL,
  `dtmUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_aktor_aknop`
--

INSERT INTO `tr_aktor_aknop` (`id_aktor_aknop`, `id_aknop_trAknop`, `nama_aktor`, `anggaran_aktor`, `txtInsertedBy`, `dtmInsertedDate`, `txtUpdatedBy`, `dtmUpdatedDate`) VALUES
(1, 1, 'Staff', 3400000, 'admin', '2023-10-03 09:12:55', '', '0000-00-00 00:00:00'),
(2, 1, 'Tukang Pacul', 1500000, 'admin', '2023-10-03 09:12:55', '', '0000-00-00 00:00:00'),
(3, 1, 'Pengawas', 6500000, 'admin', '2023-10-03 09:12:55', '', '0000-00-00 00:00:00'),
(4, 1, 'Mandor', 7000000, 'admin', '2023-10-03 09:12:55', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_aknop`
--
ALTER TABLE `tr_aknop`
  ADD PRIMARY KEY (`id_aknop`);

--
-- Indexes for table `tr_aktor_aknop`
--
ALTER TABLE `tr_aktor_aknop`
  ADD PRIMARY KEY (`id_aktor_aknop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_aknop`
--
ALTER TABLE `tr_aknop`
  MODIFY `id_aknop` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_aktor_aknop`
--
ALTER TABLE `tr_aktor_aknop`
  MODIFY `id_aktor_aknop` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
